<?php

namespace Survey\Core\Event;

use Doctrine\ORM\EntityManager;
use Survey\Core\Entity\Answer;
use Survey\Core\Entity\Question;
use Survey\Core\Entity\Test;
use Laminas\EventManager\Event;
use Laminas\EventManager\EventManager;

class AnswerEvent
{
    const EVENT_CREATE = 'EVENT_CREATE';
    const EVENT_DELETE = 'EVENT_DELETE';

    private $eventManager;
    private $questionEvent;

    public function __construct(
        EventManager $eventManager,
        EntityManager $entityManager,
        QuestionEvent $qe
    )
    {
        $this->eventManager = $eventManager;
        $this->entityManager = $entityManager;
        $this->questionEvent = $qe;

        $this->initEvents();
    }

    private function initEvents()
    {
        $this->eventManager->attach(self::EVENT_CREATE, function (Event $event) {
            /** @var Question $question */
            $question = $event->getParam('question');
            /** @var Test $test */
            $test = $question->getTest();

            if (empty($question))
                return;

            $isCanActivate = false;
            switch ($test->getType()) {
                case Test::TYPE_RATING:
                    $isCanActivate = count($question->getAllActiveAnswersForRating()->toArray()) === 1;
                    break;

                case Test::TYPE_TEST:
                    $isCanActivate = !empty($this->entityManager
                        ->getRepository(Answer::class)
                        ->findAllActiveTruthAnswerOfQuestion($question->getId()));
                    break;

                case Test::TYPE_FEEDBACK:
                    $isCanActivate = count($question->getAllActiveAnswersForFeedback()->toArray()) > 0;
                    break;

            }

            if ($isCanActivate) {
                $question->setState(Question::STATE_ACTIVE);
                $this->entityManager->flush();

                $this->questionEvent->trigger(QuestionEvent::EVENT_ACTIVE, ['test' => $test]);
            }
        });

        $this->eventManager->attach(self::EVENT_DELETE, function (Event $event) {
            /** @var Question $question */
            $question = $event->getParam('question');
            /** @var Test $test */
            $test = $question->getTest();

            if (empty($question))
                return;

            $isCanDeactivate = false;
            switch ($test->getType()) {
                case Test::TYPE_RATING:
                    $isCanDeactivate = count($question->getAllActiveAnswersForRating()->toArray()) !== 1;
                    break;

                case Test::TYPE_TEST:
                    $isCanDeactivate = empty($this->entityManager
                        ->getRepository(Answer::class)
                        ->findAllActiveTruthAnswerOfQuestion($question->getId()));
                break;

                case Test::TYPE_FEEDBACK:
                    $isCanDeactivate = count($question->getAllActiveAnswersForFeedback()->toArray()) < 1;
                    break;
            }

            if ($isCanDeactivate) {
                $question->setState(Question::STATE_DRAFT);

                $this->entityManager->flush();

                $this->questionEvent->trigger(QuestionEvent::EVENT_DRAFT, ['test' => $test]);
            }
        });
    }

    public function trigger($eventName, $data)
    {
        $this->eventManager->trigger($eventName, null, $data);
    }
}
