<?php

namespace Survey\Core\Event;

use Doctrine\ORM\EntityManager;
use Laminas\EventManager\EventManager;
use Laminas\EventManager\Event;
use Survey\Core\Entity\MessageTemplate;
use Survey\Core\Entity\TestingTest;
use Survey\Core\Entity\User;
use Survey\Core\Service\EventObserverInterface;
use Predis\Client as PredisClient;

class TestingTestEvent
{
    const EVENT_ATTACH_USER = 'EVENT_ATTACH_USER';
    const EVENT_DETACH_USER = 'EVENT_DETACH_USER';
    const EVENT_USER_FINISHED_TEST = 'EVENT_USER_FINISHED_TEST';

    private $eventManager;
    private $entityManager;
    private $redisClient;
    private $config;
    
    private $countSendMessage;

    public function __construct(
        EventManager $eventManager,
        EntityManager $em,
        PredisClient $redisClient,
        $config
    ) {
        $this->eventManager = $eventManager;
        $this->entityManager = $em;
        $this->redisClient = $redisClient;
        $this->config = $config;

        $this->initEvents();
    }

    private function initEvents()
    {
        $this->eventManager->attach(self::EVENT_ATTACH_USER, function (Event $event) {
            /** @var User $user */
            $user = $test = $event->getParam('user');
            /** @var TestingTest $testingTest */
            $testingTest = $test = $event->getParam('testing_test');
            // send message what user attached to tht test
            if ($user instanceof User && $testingTest instanceof TestingTest) {
                //TODO if test started and not finished
                if ($testingTest->getTestStatus() === TestingTest::STATUS_IN_WORK) {
                    // send message to user what remind finished test
                    $template =  $testingTest->getNotifyReminderMessageTemplate();
                    if (!empty($template)) {
                        $queueKey = $this->config['redis_config']['redis_query_key'];
                        if (empty($queueKey))
                            $queueKey = 'REDIS_QUEUE_KEY';

                        if ($this->isCanSendMessage()) {
                            //send email to supervisor
                            $this->redisClient->lpush(
                                $queueKey,
                                json_encode([
                                    'event' => MessageTemplate::REDIS_QUEUE_EVENT__SEND_EMAIL,
                                    'data' => [
                                        'user_id' => $user->getId(),
                                        'template_id' => $template->getId(),
                                        'testing_test_id' => $testingTest->getid(),
                                    ]
                                ])
                            );
                        }
                    }
                }
            }
        });

        $this->eventManager->attach(self::EVENT_DETACH_USER, function (Event $event) {
            // when detach user from the test
        });
    }

    public function attach($eventName, callable $observer)
    {
        $this->eventManager->attach($eventName, $observer);
    }

    public function trigger($eventName, $data)
    {
        $this->eventManager->trigger($eventName, null, $data);
    }
    
    private function isCanSendMessage(): bool
    {
        if ($this->config['development_mode']) {
            return $this->config['limit_count_send_message'] > $this->countSendMessage;
        }
        return true;
    }
}
