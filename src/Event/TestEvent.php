<?php

namespace Survey\Core\Event;

use Doctrine\ORM\EntityManager;
use Survey\Core\Entity\Question;
use Survey\Core\Entity\Test;
use Laminas\EventManager\Event;
use Laminas\EventManager\EventManager;

class TestEvent
{
    const EVENT_DELETE = 'EVENT_DELETE';

    private $eventManager;
    private $entityManager;

    public function __construct(EventManager $eventManager, EntityManager $entityManager)
    {
        $this->eventManager = $eventManager;
        $this->entityManager = $entityManager;

        $this->initEvents();
    }

    private function initEvents()
    {
        $this->eventManager->attach(self::EVENT_DELETE, function (Event $event) {
            /** @var Test $test */
            $test = $event->getParam('test');

            if (empty($test))
                return;

            $questions = $test->getAllQuestions();
            /** @var Question $question */
            foreach ($questions as $question) {
                $question->setState(Question::STATE_DELETE);
            }

            $this->entityManager->flush();
        });
    }

    public function trigger($eventName, $data)
    {
        $this->eventManager->trigger($eventName, null, $data);
    }
}