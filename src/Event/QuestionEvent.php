<?php

namespace Survey\Core\Event;

use Doctrine\ORM\EntityManager;
use Survey\Core\Entity\Test;
use Laminas\EventManager\Event;
use Laminas\EventManager\EventManager;

class QuestionEvent
{
    const EVENT_ACTIVE = 'EVENT_ACTIVE';
    const EVENT_DRAFT = 'EVENT_DRAFT';
    const EVENT_DELETE = 'EVENT_DELETE';

    private $eventManager;
    private $entityManager;

    public function __construct(EventManager $eventManager, EntityManager $entityManager)
    {
        $this->eventManager = $eventManager;
        $this->entityManager = $entityManager;

        $this->initEvents();
    }

    private function initEvents()
    {
        $this->eventManager->attach(self::EVENT_DELETE, function (Event $event) {
            /** @var Test $test */
            $test = $event->getParam('test');

            $this->testToDraft($test);
        });

        $this->eventManager->attach(self::EVENT_DRAFT, function (Event $event) {
            /** @var Test $test */
            $test = $event->getParam('test');

            $this->testToDraft($test);
        });

        $this->eventManager->attach(self::EVENT_ACTIVE, function (Event $event) {
            /** @var Test $test */
            $test = $event->getParam('test');

            $this->testToActive($test);
        });
    }

    private function testToDraft(?Test $test)
    {
        if (empty($test))
            return;

        if (empty($test->getAllActiveQuestions()->toArray())) {
            $test->setState(Test::STATE_DRAFT);

            $this->entityManager->flush();
        }
    }
    
    private function testToActive(?Test $test)
    {
        if (empty($test))
            return;

        if (!empty($test->getAllActiveQuestions()->toArray())) {
            $test->setState(Test::STATE_ACTIVE);

            $this->entityManager->flush();
        }
    }

    public function trigger($eventName, $data)
    {
        $this->eventManager->trigger($eventName, null, $data);
    }
}
