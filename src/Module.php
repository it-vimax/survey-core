<?php

namespace Survey\Core;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Interop\Container\ContainerInterface;
use Survey\Core\Event\AnswerEvent;
use Survey\Core\Event\Factory\AnswerEventFactory;
use Survey\Core\Event\Factory\QuestionEventFactory;
use Survey\Core\Event\Factory\TestEventFactory;
use Survey\Core\Event\Factory\TestingTestEventFactory;
use Survey\Core\Event\QuestionEvent;
use Survey\Core\Event\TestEvent;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Survey\Core\Event\TestingTestEvent;
use Survey\Core\EventObserver\Factory\TestingTestNotifyIncorrectObserverFactory;
use Survey\Core\EventObserver\TestingTestNotifyIncorrectObserver;
use Survey\Core\Helper\Log;
use Survey\Core\Service\Factory\MessageManagerFactory;
use Survey\Core\Service\MessageManager;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        return [
            'doctrine' => [
                'driver' => [
                    __NAMESPACE__ . '_driver' => [
                        'class' => AnnotationDriver::class,
                        'cache' => 'array',
                        'paths' => [__DIR__ . '/Entity'],
                    ],
                    'orm_default' => [
                        'drivers' => [
                            __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                        ]
                    ]
                ],
            ],
        ];
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                TestEvent::class => TestEventFactory::class,
                QuestionEvent::class => QuestionEventFactory::class,
                AnswerEvent::class => AnswerEventFactory::class,
                TestingTestEvent::class => TestingTestEventFactory::class,

                MessageManager::class => MessageManagerFactory::class,

                Log::class => function (ContainerInterface $container) {
                    $config = $container->get('config');
                    return new Log($config);
                },

                TestingTestNotifyIncorrectObserver::class => TestingTestNotifyIncorrectObserverFactory::class,
            ],
        ];
    }
}
