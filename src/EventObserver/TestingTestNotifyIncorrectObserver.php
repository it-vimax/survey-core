<?php

namespace Survey\Core\EventObserver;

use Doctrine\ORM\EntityManager;
use Laminas\Serializer\Adapter\PhpSerialize;
use Survey\Core\Entity\Department;
use Survey\Core\Entity\MessageTemplate;
use Survey\Core\Entity\Position;
use Survey\Core\Entity\TestingTest;
use Survey\Core\Entity\User;
use Survey\Core\Entity\UserQuestionAnswer;
use Survey\Core\Service\EventObserverInterface;
use Predis\Client as PredisClient;

class TestingTestNotifyIncorrectObserver implements EventObserverInterface
{
    private $entityManager;
    private $redisClient;
    private $config;

    public function __construct(EntityManager $em, PredisClient $redisClient, $config)
    {
        $this->entityManager = $em;
        $this->redisClient = $redisClient;
        $this->config = $config;
    }

    public function handler($data)
    {
        $queueKey = $this->config['redis_config']['redis_query_key'];
        if (empty($queueKey))
            $queueKey = 'REDIS_QUEUE_KEY';

        if ((isset($data['testing_test']) && $data['testing_test'] instanceof TestingTest)
            && (isset($data['user']) && $data['user'] instanceof User)) {
            /** @var User $user */
            $user = $data['user'];
            /** @var TestingTest $testingTest */
            $testingTest = $data['testing_test'];

            // get a log of the testing test
            /** @var UserQuestionAnswer[] $userQuestionAnswers */
            $userQuestionAnswers = $this->entityManager
                ->getRepository(UserQuestionAnswer::class)
                ->findWithMaxAttempt($user, $testingTest);

            //Check if there are errors in the answers?
            foreach ($userQuestionAnswers as $userQuestionAnswer) {
                if (!$userQuestionAnswer->getAnswer()->isTruth()) {
                    // is errors exist:
                    // 1. get an email template for send to supervisor
                    if (!$user->getPosition()->isSupervisor()) {
                        /** @var MessageTemplate $supervisorMessageTemplate */
                        $supervisorMessageTemplate = $testingTest->getNotifyIncorrectTestBySupervisor();
                        if (!empty($supervisorMessageTemplate)) {
                            /** @var Department $department */
                            $department = $user->getDepartment();
                            /** @var Position[] $supervisorPositions */
                            $supervisorPositions = $department->getSupervisorPositions();
                            foreach ($supervisorPositions as $position) {
                                foreach ($position->getUsersByDepartment($department) as $supervisor) {
                                    $this->redisClient->lpush(
                                        $queueKey,
                                        json_encode([
                                            'event' => MessageTemplate::REDIS_QUEUE_EVENT__SEND_EMAIL,
                                            'data' => [
                                                'user_id' => $supervisor->getId(),
                                                'template_id' => $supervisorMessageTemplate->getId(),
                                                'testing_test_id' => $testingTest->getId(),
                                                'employee_id' => $user->getId(),
                                            ]
                                        ])
                                    );
                                }
                            }
                        }
                    }

                    // 3. get en email template for send to user
                    /** @var MessageTemplate $userMessageTemplate */
                    $userMessageTemplate = $testingTest->getNotifyIncorrectTestByUser();
                    if (!empty($userMessageTemplate)) {
                        // 4. send email to user
                        $this->redisClient->lpush(
                            $queueKey,
                            json_encode([
                                'event' => MessageTemplate::REDIS_QUEUE_EVENT__SEND_EMAIL,
                                'data' => [
                                    'user_id' => $user->getId(),
                                    'template_id' => $userMessageTemplate->getId(),
                                    'testing_test_id' => $testingTest->getid(),
                                ]
                            ])
                        );
                    }
                    break;
                }
            }
        }
    }
}
