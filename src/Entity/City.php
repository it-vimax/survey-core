<?php

namespace Survey\Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\Survey\Core\Repository\CityRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="cities")
 */
class City
{
    use TimestampableEntityTraid;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(name="name")
     */
    protected $name;
    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\SubdivisionCity", mappedBy="city")
     * @ORM\JoinColumn(name="id", referencedColumnName="city_id")
     */
    protected $subdivisionsCity;

    public function __construct()
    {
        $this->subdivisionsCity = new ArrayCollection();
    }
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return SubdivisionCity
     */
    public function getSubdivisionsCity()
    {
        return $this->subdivisionsCity;
    }

    public function addSubdivisionCity(SubdivisionCity $subdivisionCity)
    {
        $this->subdivisionsCity[] = $subdivisionCity;
    }
}
