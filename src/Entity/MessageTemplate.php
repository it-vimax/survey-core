<?php

namespace Survey\Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Survey\Core\Utils\MessageShortCode;

/**
 * @ORM\Entity(repositoryClass="\Survey\Core\Repository\MessageTemplateRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="message_templates")
 */
class MessageTemplate
{
    use TimestampableEntityTraid;

    const REDIS_QUEUE_EVENT__SEND_EMAIL = "REDIS_QUEUE_EVENT__SEND_EMAIL";

    //email types
    const TYPE_INCORRECT_ANSWER_NOTIFY_BY_SUPERVISOR = 1;
    const TYPE_INCORRECT_ANSWER_NOTIFY_BY_USER = 2;
    const TYPE_TEST_NOTIFY_START = 3;
    const TYPE_TEST_NOTIFY_REMINDER = 4;
    // email format
    const FORMAT_TEXT = 1;
    const FORMAT_EMAIL = 2;

    // types form volia service manager
    const SERVICE_MANAGER_TYPE_EMAIL = 'EMAIL';

    /** @var MessageShortCode */
    private $emailShortCode;
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(name="type", type="integer")
     */
    protected $type;
    /**
     * @ORM\Column(name="format", type="integer")
     */
    protected $format;
    /**
     * @ORM\Column(name="name", type="string")
     */
    protected $name;
    /**
     * @ORM\Column(name="subject", type="string")
     */
    protected $subject;
    /**
     * @ORM\Column(name="body", type="string")
     */
    protected $body;
    /**
     * @ORM\Column(name="description", type="string")
     */
    protected $description;
    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\TestingTest", mappedBy="notifyStart")
     * @ORM\JoinColumn(name="id", referencedColumnName="notify_start_id")
     */
    protected $notifyStartTestingTests;
    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\TestingTest", mappedBy="notifyIncorrectTestBySupervisor")
     * @ORM\JoinColumn(name="id", referencedColumnName="notify_incorrect_test_supervisor_id")
     */
    protected $notifyIncorrectTestSupervisorTestingTests;
    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\TestingTest", mappedBy="notifyIncorrectTestByUser")
     * @ORM\JoinColumn(name="id", referencedColumnName="notify_incorrect_test_user_id")
     */
    protected $notifyIncorrectTestUserTestingTests;
    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\TestingTest", mappedBy="notifyReminder")
     * @ORM\JoinColumn(name="id", referencedColumnName="notify_reminder_id")
     */
    protected $notifyReminderTestingTests;

    public function __construct()
    {
        $this->notifyIncorrectTestSupervisorTestingTests = new ArrayCollection();
        $this->notifyIncorrectTestUserTestingTests = new ArrayCollection();
        $this->notifyStartTestingTests = new ArrayCollection();
        $this->notifyReminderTestingTests = new ArrayCollection();
    }
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }
    /**
     * @return mixed
     */
    public function getSubject()
    {
        if ($this->format === self::FORMAT_TEXT) {
            return strip_tags($this->subject);
        }
        return $this->subject;
    }
    /**
     * @param mixed $subject
     */
    public function setSubject($subject): void
    {
        $this->subject = $subject;
    }
    /**
     * @return mixed
     */
    public function getBody()
    {
        if ($this->format === self::FORMAT_TEXT) {
            return strip_tags($this->body);
        }
        return $this->body;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body): void
    {
        $this->body = $body;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type): void
    {
        $this->type = $type;
    }

    public function getFormat()
    {
        return $this->format;
    }

    public function setFormat($format): void
    {
        $this->format = $format;
    }

    public function isFormatText()
    {
        return $this->format === self::FORMAT_TEXT ? true : false;
    }

    public function isFormatEmail()
    {
        return $this->format === self::FORMAT_EMAIL ? true : false;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    //NotifyStartTestingTest
    public function addNotifyStartTestingTest(TestingTest $testingTest)
    {
        $this->notifyStartTestingTests[] = $testingTest;
    }

    public function getNotifyStartTestingTest()
    {
        return $this->notifyStartTestingTests;
    }

    public function removeNotifyStartElement(TestingTest $testingTest)
    {
        $this->notifyStartTestingTests->removeElement($testingTest);
    }

    //NotifyReminderTestingTest
    public function addNotifyReminderTestingTest(TestingTest $testingTest)
    {
        $this->notifyReminderTestingTests[] = $testingTest;
    }

    public function getNotifyReminderTestingTest()
    {
        return $this->notifyReminderTestingTests;
    }

    public function removeNotifyReminderElement(TestingTest $testingTest)
    {
        $this->notifyReminderTestingTests->removeElement($testingTest);
    }

    //NotifyIncorrectTestSupervisor
    public function addNotifyIncorrectTestSupervisorTestingTests(TestingTest $testingTest)
    {
        $this->notifyIncorrectTestSupervisorTestingTests[] = $testingTest;
    }

    public function getNotifyIncorrectTestSupervisorTestingTests()
    {
        return $this->notifyIncorrectTestSupervisorTestingTests;
    }

    public function removeNotifyIncorrectTestSupervisorElement(TestingTest $testingTest)
    {
        $this->notifyIncorrectTestSupervisorTestingTests->removeElement($testingTest);
    }

    //NotifyIncorrectTestUserTestingTests
    public function addNotifyIncorrectTestUserTestingTests(TestingTest $testingTest)
    {
        $this->notifyIncorrectTestUserTestingTests[] = $testingTest;
    }

    public function getNotifyIncorrectTestUserTestingTests()
    {
        return $this->notifyIncorrectTestUserTestingTests;
    }

    public function removeNotifyIncorrectTestUserElement(TestingTest $testingTest)
    {
        $this->notifyIncorrectTestUserTestingTests->removeElement($testingTest);
    }

    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'body' => $this->body,
            'type' => $this->type,
            'format' => $this->format,
            'name' => $this->name,
            'subject' => $this->subject,
            'description' => $this->description,
        ];
    }
    
    public function exchangeArray(array $data)
    {
        if (isset($data['id'])) {
            $this->id = $data['id'];
        }

        if (isset($data['body'])) {
            $this->body = $data['body'];
        }

        if (isset($data['name'])) {
            $this->name = $data['name'];
        }

        if (isset($data['subject'])) {
            $this->subject = $data['subject'];
        }

        if (isset($data['description'])) {
            $this->description = $data['description'];
        }

        if (isset($data['type'])) {
            $this->type = $data['type'];
        }

        if (isset($data['format'])) {
            $this->format = $data['format'];
        }
    }
    
    public static function getAllTypes()
    {
        return [
            self::TYPE_INCORRECT_ANSWER_NOTIFY_BY_SUPERVISOR => 'Сообщение супервизору при ошибке в тесте',
            self::TYPE_INCORRECT_ANSWER_NOTIFY_BY_USER => 'Сообщение пользователю при ошибке в тесте',
            self::TYPE_TEST_NOTIFY_START => 'Информирование об начале теста',
            self::TYPE_TEST_NOTIFY_REMINDER => 'Информирование об участвии в тесте',
        ];
    }

    public static function getAllFormats()
    {
        return [
            self::FORMAT_EMAIL => 'Email',
//            self::FORMAT_TEXT => 'Текстовое сообщение',
        ];
    }

    public function getNameType()
    {
        foreach (MessageTemplate::getAllTypes() as $id => $name) {
            if ($id === $this->type) {
                return $name;
            }
        }

        return 'Type "' . $this->type . '" is not define';
    }

    public function getNameFormat()
    {
        foreach (MessageTemplate::getAllFormats() as $id => $name) {
            if ($id === $this->format) {
                return $name;
            }
        }

        return 'Format "' . $this->format . '" is not define';
    }

    public static function getHaystackTypes()
    {
        return [
            self::TYPE_INCORRECT_ANSWER_NOTIFY_BY_SUPERVISOR,
            self::TYPE_INCORRECT_ANSWER_NOTIFY_BY_USER,
            self::TYPE_TEST_NOTIFY_START,
            self::TYPE_TEST_NOTIFY_REMINDER,
        ];
    }

    public static function getHaystackFormats()
    {
        return [
            self::FORMAT_EMAIL,
//            self::FORMAT_TEXT,
        ];
    }
    
    public function initMessageShortCode(MessageShortCode $emailShortCode)
    {
        $this->emailShortCode = $emailShortCode;
    }
    
    public function getBodyWithShortCode()
    {
        if (!empty($this->emailShortCode)) {
            return $this->emailShortCode->formattingTemplate($this->body);
        }

        return $this->body;
    }
    
    public function getSubjectWithShortCode()
    {
        if (!empty($this->emailShortCode)) {
            return $this->emailShortCode->formattingTemplate($this->subject);
        }

        return $this->subject;
    }

    public function getServiceManagerMessageType()
    {
        switch ($this->format) {
            case self::FORMAT_EMAIL:
                return self::SERVICE_MANAGER_TYPE_EMAIL;
                
            default:
                throw new \Exception('Messge format ' . $this->format . ' is not allowed!');
        }
    }
}
