<?php

namespace Survey\Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\Survey\Core\Repository\QuestionRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="organizations")
 */
class Organization
{
    use TimestampableEntityTraid;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(name="name")
     */
    protected $name;
    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\Department", mappedBy="organization")
     * @ORM\JoinColumn(name="id", referencedColumnName="organization_id")
     */
    protected $departments;
    /**
     * @ORM\Column(name="global_id")
     */
    private $globalId;

    public function __construct()
    {
        $this->departments = new ArrayCollection();
    }

    public function getGlobalId()
    {
        return $this->globalId;
    }

    public function setGlobalId($globalId)
    {
        $this->globalId = $globalId;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDepartments()
    {
        return $this->departments;
    }

    public function addDepartment(Department $department)
    {
        $this->departments[] = $department;
    }
}
