<?php

namespace Survey\Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\Survey\Core\Repository\DepartmentRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="departments")
 */
class Department
{
    use TimestampableEntityTraid;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(name="name")
     */
    protected $name;
    /**
     * @var Organization
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\Organization", inversedBy="departments")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     */
    protected $organization;
    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\User", mappedBy="department")
     * @ORM\JoinColumn(name="id", referencedColumnName="department_id")
     */
    protected $users;
    /**
     * @ORM\Column(name="global_id")
     */
    protected $globalId;

    /**
     * @ORM\ManyToMany(targetEntity="\Survey\Core\Entity\Position", inversedBy="departments")
     * @ORM\JoinTable(name="departments_positions",
     *      joinColumns={@ORM\JoinColumn(name="department_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="position_id", referencedColumnName="id")}
     *      )
     */
    private $positions;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->positions = new ArrayCollection();
    }

    public function getGlobalId()
    {
        return $this->globalId;
    }

    public function setGlobalId($globalId)
    {
        $this->globalId = $globalId;
    }

    /**
     * @return User[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $user
     */
    public function addUser(User $user): void
    {
        $this->users[] = $user;
    }

    public function removeElement(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }
    
    public function setOrganization(Organization $organization)
    {
        $this->organization = $organization;
        $organization->addDepartment($this);
    }

    /**
     * @return Position[]
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * @return Position[]
     */
    public function getSupervisorPositions()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('isSupervisor', 1));

        return $this->positions->matching($criteria);
    }

    public function addPosition(Position $position)
    {
        $predicate = function ($key, Position $_position) use ($position) {
            return $_position->getId() === $position->getId();
        };

        if (!$this->positions->exists($predicate)) {
            $this->positions[] = $position;
            $position->addDepartment($this);
        }
    }

    public function removePosition(Position $position)
    {
        $this->positions->removeElement($position);
    }

    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'global_id' => $this->globalId,
            'name' => $this->name,
            'organization_id' => $this->organization->getId(),
        ];
    }
}
