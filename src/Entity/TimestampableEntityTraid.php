<?php

namespace Survey\Core\Entity;

use DateTime;

trait TimestampableEntityTraid
{
    /**
     * @ORM\Column(name="create_at", type="datetime")
     */
    private $createAt;
    /**
     * @ORM\Column(name="update_at", type="datetime")
     */
    private $updateAt;

    /**
     * @return DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    public function setCreateAt(DateTime $createAt)
    {
        return $this->createAt = $createAt;
    }

    /**
     *  @ORM\PrePersist
     */
    public function createAtListener(): void
    {
        if (empty($this->createAt)) {
            $this->createAt = new DateTime();
            $this->updateAt = new DateTime();
        }
    }

    /**
     * @return DateTime
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    public function setUpdateAt(DateTime $updateAt)
    {
        return $this->updateAt = $updateAt;
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateAtListener(): void
    {
        $this->updateAt = new DateTime();
    }
}
