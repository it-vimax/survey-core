<?php

namespace Survey\Core\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * @ORM\Entity(repositoryClass="\Survey\Core\Repository\QuestionRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="questions")
 */
class Question
{
    use TimestampableEntityTraid;

    const IS_MULTI_ANSWER_FALSE = 0;
    const IS_MULTI_ANSWER_TRUE = 1;

    const STATE_DELETE = 0; // Удаленный вопрос
    const STATE_ACTIVE = 1; // Активный вопрос
    const STATE_DRAFT = 2; // Если активный вопрос, но в нем нету ни одного ответа, возвращаем состояние черновика

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(name="weight", type="integer")
     */
    private $weight;
    /**
     * @var Test
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\Test", inversedBy="questions")
     * @ORM\JoinColumn(name="test_id", referencedColumnName="id")
     */
    private $test;
    /**
     * @ORM\Column(name="state", type="integer")
     */
    private $state;
    /**
     * @ORM\Column(name="is_multi_answer", type="integer")
     */
    private $isMultiAnswer;
    /**
     * @ORM\Column(name="value")
     */
    private $value;
    /**
     * @ORM\Column(name="code")
     */
    private $code;
    /**
     * @ORM\Column(name="img")
     */
    private $img;

    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\Answer", mappedBy="question")
     * @ORM\JoinColumn(name="id", referencedColumnName="question_id")
     */
    private $answers;

    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\UserQuestionAnswer", mappedBy="question")
     * @ORM\JoinColumn(name="id", referencedColumnName="question_id")
     */
    protected $userQuestionAnswers;

    public function __construct()
    {
        $this->answers = new ArrayCollection();
        $this->userQuestionAnswers = new ArrayCollection();
    }

    public function getUserQuestionAnswers()
    {
        $criteria = Criteria::create();

        return $this->userQuestionAnswers->matching($criteria);
    }

    public function addUserQuestionAnswer(UserQuestionAnswer $userQuestionAnswer)
    {
        $this->userQuestionAnswers[] = $userQuestionAnswer;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return Answer[]
     */
    public function getAllDontRemoveAnswers($isActiveAnswer = true)
    {
        $criteria = Criteria::create();
        if ($isActiveAnswer) {
            $criteria->where(Criteria::expr()->neq('state', Answer::STATE_DELETE));
        }
        return $this->answers->matching($criteria);
    }

    /**
     * @return Answer[]
     */
    public function getAllActiveAnswers()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('state', Answer::STATE_ACTIVE));
        $criteria->orderBy(['weight' => Criteria::ASC]);

        return $this->answers->matching($criteria);
    }

    /**
     * @return Answer[]
     */
    public function getAllActiveAnswersForRating()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('state', Answer::STATE_ACTIVE));
        $criteria->andWhere(Criteria::expr()->eq('type', Answer::TYPE_RATING));
        $criteria->orderBy(['weight' => Criteria::ASC]);

        return $this->answers->matching($criteria);
    }

    /**
     * @return Answer[]
     */
    public function getAllActiveAnswersForFeedback()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('state', Answer::STATE_ACTIVE));
        $criteria->andWhere(Criteria::expr()->orX(
            Criteria::expr()->eq('type', Answer::TYPE_FEEDBACK),
            Criteria::expr()->eq('type', Answer::TYPE_FEEDBACK_CUSTOM))
        );
        $criteria->orderBy(['weight' => Criteria::ASC]);

        return $this->answers->matching($criteria);
    }

    /**
     * @return Answer
     */
    public function getRatingValue()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('state', Answer::STATE_ACTIVE));

        $result = $this->answers->matching($criteria)->toArray();

        return reset($result);
    }

    /**
     * @return Answer[]
     */
    public function getAllActiveAnswersTruth()
    {
        $criteria = Criteria::create();
        $criteria
            ->where(Criteria::expr()->eq('state', Answer::STATE_ACTIVE))
            ->andWhere(Criteria::expr()->eq('point', Answer::STATUS_TRUTH));

        return $this->answers->matching($criteria);
    }

    /**
     * @param mixed $answer
     */
    public function addAnswer(Answer $answer)
    {
        $this->answers[] = $answer;
    }

    /**
     * @return Test
     */
    public function getTest(): Test
    {
        return $this->test;
    }

    /**
     * @param Test $test
     */
    public function setTest(Test $test): void
    {
        $this->test = $test;
        $test->addQuestion($this);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function getIsMultiAnser()
    {
        return $this->isMultiAnswer;
    }

    /**
     * @param mixed $isMultiAnswer
     */
    public function setIsMultiAnser($isMultiAnswer): void
    {
        $this->isMultiAnswer = $isMultiAnswer;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state): void
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     */
    public function setImg($img): void
    {
        $this->img = $img;
    }

    public function validateParentTest(Test $test)
    {
        if ($this->getTest()->getId() !== $test->getId()) {
            throw new Exception('Test id ' . $test->getId() . ' is not parent for question id ' . $this->id);
        }
    }

    /**
     * Этот массив для загрузки даных с модели
     * в форму методом $form->bind($question)
     */
    public function exchangeArray(array $data)
    {
        if (isset($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['weight'])) {
            $this->weight = $data['weight'];
        }
        if (isset($data['code'])) {
            $this->code = $data['code'];
        }
        if (isset($data['state'])) {
            $this->state = $data['state'];
        }
        if (isset($data['value'])) {
            $this->value = $data['value'];
        }
        if (isset($data['is_multi_answer'])) {
            $this->isMultiAnswer = $data['is_multi_answer'];
        }
        if (isset($data['img'])) {
            $this->img = $data['img'];
        }
    }

    /**
     * Для валидации загруженой формы
     */
    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'test_id' => $this->test->getId(),
            'weight' => $this->weight,
            'state' => $this->state,
            'code' => $this->code,
            'value' => $this->value,
            'is_multi_answer' => $this->isMultiAnswer,
            'img' => $this->img,
        ];
    }
}
