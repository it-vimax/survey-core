<?php

namespace Survey\Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\Survey\Core\Repository\UserQuestionAnswerRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="user_question_answers")
 */
class UserQuestionAnswer
{
    use TimestampableEntityTraid;

    const RESULT_PERFECT = 100; //Идеально
    const RESULT_GOOD = 75; //Хорошо
    const RESULT_FINE = 50; //Нормально
    const RESULT_POORLY = 25; //Плохо
    const RESULT_AWFUL = 1; //Ужасно
    const RESULT_ZERO = 0; //Ноль

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\User", inversedBy="userQuestionAnswers")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;
    /**
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\TestingTest", inversedBy="userQuestionAnswers")
     * @ORM\JoinColumn(name="testing_test_id", referencedColumnName="id")
     */
    protected $testingTest;
    /**
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\Question", inversedBy="userQuestionAnswers")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    protected $question;
    /**
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\Answer", inversedBy="userQuestionAnswers")
     * @ORM\JoinColumn(name="answer_id", referencedColumnName="id")
     */
    protected $answer;

    /**
     * @ORM\Column(name="custom_answer")
     */
    protected $customAnswer;

    /**
     * @ORM\Column(name="attempt", type="integer")
     */
    protected $attempt;

    /**
     * @ORM\Column(name="cycle", type="integer")
     */
    protected $cycle;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
        $user->addUserQuestionAnswer($this);
    }

    /**
     * @return TestingTest
     */
    public function getTestingTest(): TestingTest
    {
        return $this->testingTest;
    }

    /**
     * @param User $testingTest
     */
    public function setTestingTest(TestingTest $testingTest): void
    {
        $this->testingTest = $testingTest;
        $testingTest->addUserQuestionAnswer($this);
    }

    /**
     * @return Question
     */
    public function getQuestion(): Question
    {
        return $this->question;
    }

    /**
     * @param Question $question
     */
    public function setQuestion(Question $question): void
    {
        $this->question = $question;
        $question->addUserQuestionAnswer($this);
    }

    public function getAttempt()
    {
        return $this->attempt;
    }

    public function setAttempt($attempt)
    {
        $this->attempt = $attempt;
    }

    /**
     * @return mixed
     */
    public function getCycle()
    {
        return $this->cycle;
    }

    /**
     * @param mixed $cycle
     */
    public function setCycle($cycle): void
    {
        $this->cycle = $cycle;
    }

    /**
     * @return Answer
     */
    public function getAnswer(): ?Answer
    {
        return $this->answer;
    }

    /**
     * @param Answer $answer
     */
    public function setAnswer(Answer $answer): void
    {
        $this->answer = $answer;
        $answer->addUserQuestionAnswer($this);
    }

    /**
     * @return mixed
     */
    public function getCustomAnswer()
    {
        return $this->customAnswer;
    }

    public function setCustomAnswer($customAnswer): void
    {
        $this->customAnswer = $customAnswer;
    }

    public function getRatingAnswerBallPercent()
    {
        $customAnswerValue = (int)$this->customAnswer;
        $answerValue = (int)$this->question->getRatingValue()->getPoint();

        if ($customAnswerValue === 0) {
            return 0;
        }

        $result = $customAnswerValue / $answerValue * 100;

        return round($result);
    }

    /**
     * @return self
     */
    public static function create(
        User $user,
        TestingTest $testingTest,
        Question $question,
        ?Answer $answer,
        $attempt,
        $cycle,
        $customAnswer = null
    )
    {
        $newUserQuestionAnswer = new self();

        $newUserQuestionAnswer->setUser($user);
        $newUserQuestionAnswer->setTestingTest($testingTest);
        $newUserQuestionAnswer->setQuestion($question);

        if (!empty($answer))
            $newUserQuestionAnswer->setAnswer($answer);

        if (!empty($customAnswer))
            $newUserQuestionAnswer->setCustomAnswer($customAnswer);

        $newUserQuestionAnswer->setAttempt($attempt);
        $newUserQuestionAnswer->setCycle($cycle);

        return $newUserQuestionAnswer;
    }
}
