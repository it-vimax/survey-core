<?php

namespace Survey\Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\Survey\Core\Repository\UserFunctionRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="users_function")
 */
class UserFunction
{
    use TimestampableEntityTraid;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(name="name")
     */
    protected $name;
    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\User", mappedBy="userFunction")
     * @ORM\JoinColumn(name="id", referencedColumnName="user_function_id")
     */
    protected $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return User[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $userData
     */
    public function addUser(User $user): void
    {
        $this->users[] = $user;
    }

    public function removeElement(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
