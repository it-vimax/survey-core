<?php

namespace Survey\Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="\Survey\Core\Repository\TestRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="tests")
 */
class Test
{
    use TimestampableEntityTraid;

    const TYPE_RATING = 1; // тип теста - Опросник (набор баллов)
    const TYPE_TEST = 2; // тип теста - Тест (оценка по количеству правельных ответов)
    const TYPE_FEEDBACK = 3; // тип теста - Обратная связь от пользователя (без оценки ответов по балах)

    const STATE_DELETE = 0; // удаленный тест
    const STATE_ACTIVE = 1; // активный тест
    const STATE_DRAFT = 2; // тест еще не ктивный, так, как не имеен воросов

    const IS_RANDOM_QUESTION_FALSE = 0; // случайные вопросы отключены
    const IS_RANDOM_QUESTION_TRUE = 1; // случайные вопросы включены

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(name="title")
     */
    protected $title; // название теста
    /**
     * @ORM\Column(name="description")
     */
    protected $description; // описание теста
    /**
     * @ORM\Column(name="type", type="integer")
     */
    protected $type; // тип теста: (Опросник, Тест, Обратная связь)
    /**
     * @ORM\Column(name="state", type="integer")
     */
    protected $state; // состояние теста: Удаленный или Активный
    /**
     * @ORM\Column(name="is_random_questions", type="integer")
     */
    protected $isRandomQuestions; // вывод вопросов в случайном порядке?
    /**
     * @ORM\Column(name="count_questions", type="integer")
     */
    protected $countQuestions; // количество ответов в одном тесте (например 20 с 50 созданых)
    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\Question", mappedBy="test")
     * @ORM\JoinColumn(name="id", referencedColumnName="test_id")
     */
    protected $questions;
    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\TestingTest", mappedBy="test")
     * @ORM\JoinColumn(name="id", referencedColumnName="test_id")
     */
    protected $testingTests;
    /**
     * @ORM\Column(name="attempts_number", type="integer")
     */
    protected $attemptsNumber;
    /**
     * @ORM\Column(name="passing_score", type="integer")
     */
    protected $passingScore;
    /**
     * @ORM\Column(name="code")
     */
    protected $code;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
        $this->testingTests = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTestingTest($isActiveTestingTest = true)
    {
        $criteria = Criteria::create();
        if ($isActiveTestingTest) {
            $criteria->where(Criteria::expr()->neq('state', TestingTest::STATE_DELETE));
        }
        return $this->testingTests->matching($criteria);
    }

    /**
     * @return Testing[]
     */
    public function getAllTestingPrograms()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->neq('state', TestingTest::STATE_DELETE));
        $testingTests = $this->testingTests->matching($criteria);

        $isTestingExist = [];
        $testingArr = [];
        $testingTests->filter(function (TestingTest $testingTest) use (&$isTestingExist, &$testingArr) {
            if (empty($isTestingExist[$testingTest->getTesting()->getId()])) {
                $isTestingExist[$testingTest->getTesting()->getId()] = true;
                $testingArr[] = $testingTest->getTesting();
            }
        });

        return $testingArr;
    }

    /**
     * @param mixed $testingTest
     */
    public function addTestingTest(TestingTest $testingTest): void
    {
        $this->testingTests[] = $testingTest;
    }

    /**
     * @return Question[]
     */
    public function getAllDontRemoveQuestions($isActiveQuestion = true)
    {
        $criteria = Criteria::create();
        if ($isActiveQuestion) {
            $criteria->where(Criteria::expr()->neq('state', Question::STATE_DELETE));
        }
        return $this->questions->matching($criteria);
    }

    /**
     * @return Question[]
     */
    public function getAllActiveQuestions()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('state', Question::STATE_ACTIVE));

        return $this->questions->matching($criteria);
    }

    /**
     * @return Question[]
     */
    public function getAllQuestions()
    {
        $criteria = Criteria::create();

        return $this->questions->matching($criteria);
    }

    /**
     * @param mixed $questions
     */
    public function addQuestion(Question $questions): void
    {
        $this->questions[] = $questions;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state): void
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getIsRandomQuestions()
    {
        return $this->isRandomQuestions;
    }

    public function isRandomQuestion()
    {
        return $this->isRandomQuestions === self::IS_RANDOM_QUESTION_TRUE ? true : false;
    }

    /**
     * @param mixed $isRandomQuestions
     */
    public function setIsRandomQuestions($isRandomQuestions): void
    {
        $this->isRandomQuestions = $isRandomQuestions;
    }

    /**
     * @return mixed
     */
    public function getAllCountQuestions()
    {
        return count($this->getAllActiveQuestions());
    }

    public function getQuantityQuestion()
    {
        $getTotalQuestion = count($this->getAllActiveQuestions());
        if ($getTotalQuestion <= $this->countQuestions) {
            return $getTotalQuestion;
        }

        return $this->countQuestions;
    }

    /**
     * @param mixed $countQuestions
     */
    public function setCountQuestions($countQuestions): void
    {
        $this->countQuestions = $countQuestions;
    }

    public function getPassingScore()
    {
        return $this->passingScore;
    }

    public function setPassingScore($passingScore)
    {
        $this->passingScore = $passingScore;
    }

    public function getAttemptsNumber()
    {
        return $this->attemptsNumber;
    }

    public function setAttemptsNumber($attemptsNumber)
    {
        $this->attemptsNumber = $attemptsNumber;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function isInfinityTest()
    {
        return $this->attemptsNumber < 1 ? true : false;
    }
    
    public function isActive()
    {
        return ($this->state === self::STATE_DELETE) ? false : true;
    }

    /**
     * Этот массив для загрузки даных с модели
     * в форму методом $form->bind($test)
     */
    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'type' => $this->type,
            'is_random_questions' => $this->isRandomQuestions,
            'count_questions' => $this->countQuestions,
            'attempts_number' => $this->attemptsNumber,
            'passing_score' => $this->passingScore,
            'code' => $this->code,
        ];
    }

    /**
     * Для валидации загруженой формы
     */
    public function exchangeArray(array $data)
    {
        if (isset($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['description'])) {
            $this->description = $data['description'];
        }
        if (isset($data['title'])) {
            $this->title = $data['title'];
        }
        if (isset($data['type'])) {
            $this->type = $data['type'];
        }
        if (isset($data['is_random_questions'])) {
            $this->isRandomQuestions = $data['is_random_questions'];
        }
        if (isset($data['count_questions'])) {
            $this->countQuestions = $data['count_questions'];
        }
        if (isset($data['attempts_number'])) {
            $this->attemptsNumber = $data['attempts_number'];
        }
        if (isset($data['passing_score'])) {
            $this->passingScore = $data['passing_score'];
        }
        if (isset($data['code'])) {
            $this->code = $data['code'];
        }
    }

    public function isValidateAttemptNumber($attempt)
    {
        if ($this->isInfinityTest()) {
            return true;
        }

        if ($attempt < $this->getAttemptsNumber())
            return true;

        return false;
    }

    public function isCanFinishedTest($passingScore)
    {
        return $this->passingScore <= $passingScore ? true : false;
    }
}
