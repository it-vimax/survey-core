<?php

namespace Survey\Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * This class represents a registered user.
 * @ORM\Entity(repositoryClass="\Survey\Core\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="users")
 */
class User
{
    use TimestampableEntityTraid;

    // User status constants.
    const STATUS_ACTIVE       = 1; // Active user.
    const STATUS_RETIRED      = 2; // Retired user.

    const TESTS_PASSES = 1; // пользователь участвует в тестах
    const TESTS_COMPLETED = 2; // пользователя завершил тесты
    const TESTS_AWAITING = 3; // пользователь ожидает тесты
    const TESTS_NOT_COMPLETED = 4; // получить все тесты, у которых пользователь или участвует или будет учасвовать и еще не прошёл их

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /** 
     * @ORM\Column(name="email")  
     */
    protected $email;
    /**
     * @ORM\Column(name="photo")
     */
    protected $photo;
    /** 
     * @ORM\Column(name="full_name")  
     */
    protected $fullName;

    /** 
     * @ORM\Column(name="password")  
     */
    protected $password;

    /** 
     * @ORM\Column(name="status", type="integer")
     */
    protected $status;
        
    /**
     * @ORM\Column(name="pwd_reset_token")  
     */
    protected $passwordResetToken;
    
    /**
     * @ORM\Column(name="pwd_reset_token_creation_date")  
     */
    protected $passwordResetTokenCreationDate;

    /**
     * @ORM\ManyToMany(targetEntity="\Survey\Core\Entity\TestingTest", inversedBy="users")
     * @ORM\JoinTable(name="user_testing_test",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="testing_test_id", referencedColumnName="id")})
     */
    protected $testingTests;

    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\UserNotificationLog", mappedBy="user")
     * @ORM\JoinColumn(name="id", referencedColumnName="user_id")
     */
    protected $userNotificationLogs;

    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\UserQuestionAnswer", mappedBy="user")
     * @ORM\JoinColumn(name="id", referencedColumnName="user_id")
     */
    protected $userQuestionAnswers;

    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\Testing", mappedBy="user")
     * @ORM\JoinColumn(name="id", referencedColumnName="user_id")
     */
    protected $testing;

    /**
     * @ORM\ManyToMany(targetEntity="\Survey\Core\Entity\Role")
     * @ORM\JoinTable(name="users_roles",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     *     )
     */
    protected $roles;

    /**
     * @ORM\Column(name="global_id")
     */
    protected $globalId;

    /**
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\Position", inversedBy="users")
     * @ORM\JoinColumn(name="position_id", referencedColumnName="id")
     */
    protected $position;
    /**
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\Department", inversedBy="users")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id")
     */
    protected $department;

    /**
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\SubdivisionCity", inversedBy="users")
     * @ORM\JoinColumn(name="subdivision_city_id", referencedColumnName="id")
     */
    protected $subdivisionCity;

    /**
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\UserFunction", inversedBy="users")
     * @ORM\JoinColumn(name="user_function_id", referencedColumnName="id")
     */
    protected $userFunction;

    public function __construct()
    {
        $this->testingTests = new ArrayCollection();
        $this->userQuestionAnswers = new ArrayCollection();
        $this->userNotificationLogs = new ArrayCollection();
        $this->testing = new ArrayCollection();
        $this->roles = new ArrayCollection();
    }

    /**
     * @return SubdivisionCity
     */
    public function getSubdivisionCity()
    {
        return $this->subdivisionCity;
    }

    public function setSubdivisionCity(?SubdivisionCity $subdivisionCity)
    {
        if (empty($subdivisionCity)) {
            if (!empty($this->getSubdivisionCity())) {
                $this->getSubdivisionCity()->removeElement($this);
            }
        } else {
            $subdivisionCity->addUser($this);
        }
        $this->subdivisionCity = $subdivisionCity;
    }

    /**
     * @return SubdivisionCity
     */
    public function getUserFunction()
    {
        return $this->userFunction;
    }

    public function setUserFunction(?UserFunction $userFunction)
    {
        if (empty($userFunction)) {
            if (!empty($this->getUserFunction())) {
                $this->getUserFunction()->removeElement($this);
            }
        } else {
            $userFunction->addUser($this);
        }
        $this->userFunction = $userFunction;
    }

    /**
     * @return Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    public function setDepartment(?Department $department)
    {
        if (empty($department)) {
            if (!empty($this->getDepartment())) {
                $this->getDepartment()->removeElement($this);
            }
        } else {
            $department->addUser($this);
        }
        $this->department = $department;
    }

    /**
     * @return Position
     */
    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition(?Position $position)
    {
        if (empty($position)) {
            if (!empty($this->getPosition())) {
                $this->getPosition()->removeElement($this);
            }
        } else {
            $position->addUser($this);
        }
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getGlobalId()
    {
        return $this->globalId;
    }

    /**
     * @param mixed $globalId
     */
    public function setGlobalId($globalId)
    {
        $this->globalId = $globalId;
    }

    public function getUserQuestionAnswers()
    {
        $criteria = Criteria::create();
        
        return $this->userQuestionAnswers->matching($criteria);
    }

    public function getQuestionAnswerByTestingTest(TestingTest $testingTest)
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('testingTest', $testingTest));
        $criteria->orderBy(['attempt' => Criteria::DESC]);

        return $this->userQuestionAnswers->matching($criteria);
    }
    
    public function isPassedTestingTest(TestingTest $testingTest)
    {
        $result = $this->getQuestionAnswerByTestingTest($testingTest);

        if (!empty($result)) {
            return count($result) > 0 ? true : false;
        }

        return false;
    }

    public function getDatePassedTestingTest(TestingTest $testingTest)
    {
        if ($this->isPassedTestingTest($testingTest)) {
            /** @var UserQuestionAnswer $userQuestionAnswer */
            $userQuestionAnswer = $this->getQuestionAnswerByTestingTest($testingTest)[0];
            if (!empty($userQuestionAnswer)) {
                return $userQuestionAnswer->getCreateAt()->format('H:i d/m/Y');
            }
        }

        return false;
    }

    public function getLastTestingTestAttempt($testingTest)
    {
        $attempt = 0;

        if ($this->isPassedTestingTest($testingTest)) {
            /** @var UserQuestionAnswer $userQuestionAnswer */
            $userQuestionAnswer = $this->getQuestionAnswerByTestingTest($testingTest)[0];
            if (!empty($userQuestionAnswer)) {
                $attempt = $userQuestionAnswer->getAttempt();
            }
        }

        return $attempt;
    }
    
    public function addUserQuestionAnswer(UserQuestionAnswer $userQuestionAnswer)
    {
        $this->userQuestionAnswers[] = $userQuestionAnswer;
    }

    public function getNotificationLogs()
    {
        return $this->userNotificationLogs;
    }

    public function addNotificationToLog(UserNotificationLog $userNotificationLog)
    {
        $this->userNotificationLogs[] = $userNotificationLog;
    }

    public function getTesting()
    {
        return $this->testing;
    }

    public function addTesting(Testing $testing)
    {
        $this->testing[] = $testing;
    }

    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }
    /**
     * @return TestingTest[]
     */
    public function getTestingTests($isActive = true)
    {
        $criteria = Criteria::create();
        if ($isActive) {
            $criteria->where(Criteria::expr()->neq('state', TestingTest::STATE_DELETE));
        }
        return $this->testingTests->matching($criteria);
    }

    public function addTestingTest(TestingTest $testingTest)
    {
        $this->testingTests[] = $testingTest;
    }

    public function removeTestingTestAssociation(TestingTest $testingTest)
    {
        $this->testingTests->removeElement($testingTest);
    }

    /**
     * Returns user ID.
     * @return integer
     */
    public function getId() 
    {
        return $this->id;
    }

    /**
     * Sets user ID. 
     * @param int $id    
     */
    public function setId($id) 
    {
        $this->id = $id;
    }

    /**
     * Returns email.     
     * @return string
     */
    public function getEmail() 
    {
        return $this->email;
    }

    /**
     * Sets email.     
     * @param string $email
     */
    public function setEmail($email) 
    {
        $this->email = $email;
    }
    
    /**
     * Returns full name.
     * @return string     
     */
    public function getFullName() 
    {
        return $this->fullName;
    }       

    /**
     * Sets full name.
     * @param string $fullName
     */
    public function setFullName($fullName) 
    {
        $this->fullName = $fullName;
    }
    
    /**
     * Returns status.
     * @return int     
     */
    public function getStatus() 
    {
        return $this->status;
    }

    /**
     * Returns possible statuses as array.
     * @return array
     */
    public static function getStatusList() 
    {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_RETIRED => 'Retired'
        ];
    }    
    
    /**
     * Returns user status as string.
     * @return string
     */
    public function getStatusAsString()
    {
        $list = self::getStatusList();
        if (isset($list[$this->status]))
            return $list[$this->status];
        
        return 'Unknown';
    }    
    
    /**
     * Sets status.
     * @param int $status     
     */
    public function setStatus($status) 
    {
        $this->status = $status;
    }   
    
    /**
     * Returns password.
     * @return string
     */
    public function getPassword() 
    {
       return $this->password; 
    }
    
    /**
     * Sets password.     
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
    
    /**
     * Returns password reset token.
     * @return string
     */
    public function getPasswordResetToken()
    {
        return $this->passwordResetToken;
    }
    
    /**
     * Sets password reset token.
     * @param string $token
     */
    public function setPasswordResetToken($token) 
    {
        $this->passwordResetToken = $token;
    }
    
    /**
     * Returns password reset token's creation date.
     * @return string
     */
    public function getPasswordResetTokenCreationDate()
    {
        return $this->passwordResetTokenCreationDate;
    }
    
    /**
     * Sets password reset token's creation date.
     * @param string $date
     */
    public function setPasswordResetTokenCreationDate($date) 
    {
        $this->passwordResetTokenCreationDate = $date;
    }

    /**
     * @return Role[]
     */
    public function getRoles()
    {
        return $this->roles;
    }

    public function isRoleExist(Role $role)
    {
        /** @var Role $userRole */
        foreach ($this->roles as $userRole) {
            if ($role->getId() === $userRole->getId()) {
                return true;
            }
        }
        return false;
    }

    public function getRolesAsString()
    {
        $roleList = '';

        $count = count($this->roles);
        $i = 0;
        /** @var Role $role */
        foreach ($this->roles as $role) {
            $roleList .= $role->getName();

            if ($i < $count - 1) {
                $roleList .= ', ';
            }

            $i++;
        }

        return $roleList;
    }

    public function addRole($role)
    {
        $this->roles->add($role);
    }

    public function getArrayCopy()
    {
        $fields['email'] = $this->getEmail();
        $fields['status'] = $this->getStatus();
        $fields['full_name'] = $this->getFullName();
        $fields['global_id'] = $this->getGlobalId();

        $userRoleIds = [];
        foreach ($this->getRoles() as $role) {
            $userRoleIds[] = $role->getId();
        }
        $fields['roles'] = $userRoleIds;

        if (!empty($this->getPosition())) {
            $fields['position_id'] = $this->getPosition()->getId();
        }
        if (!empty($this->getDepartment())) {
            $fields['department_id'] = $this->getDepartment()->getId();
        }
        if (!empty($this->getSubdivisionCity())) {
            $fields['subdivision_city_id'] = $this->getSubdivisionCity()->getId();
        }

        return $fields;
    }
}
