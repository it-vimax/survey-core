<?php

namespace Survey\Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * @ORM\Entity(repositoryClass="\Survey\Core\Repository\AnswerRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="answers")
 */
class Answer
{
    use TimestampableEntityTraid;

    const TYPE_RATING = 0; // ответ оценки по шкале
    const TYPE_VARIANT = 1; // ответ с вариантами верный/ложный
    const TYPE_FEEDBACK = 2; // ответ на вопрос, не имеющий оценки (Обратная связь)
    const TYPE_FEEDBACK_CUSTOM = 3; // свой овтет на вопрос типа Обратная связь

    const STATUS_LIE = 0; // ответ не верный
    const STATUS_TRUTH = 1; // ответ верный

    const STATE_DELETE = 0; // удаленный ответ
    const STATE_ACTIVE = 1; // активный ответ

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var Question
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\Question", inversedBy="answers")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    private $question;
    /**
     * @ORM\Column(name="type", type="integer")
     */
    private $type; // тип ответа (балл, свой овтет, ответ на вопросы)
    /**
     * @ORM\Column(name="point", type="integer")
     */
    private $point; // количество баллов за один ответ
    /**
     * @ORM\Column(name="state", type="integer")
     */
    private $state; // состояние ответа (активный/не активный)
    /**
     * @ORM\Column(name="weight", type="integer")
     */
    private $weight; // порядок отображения ответа
    /**
     * @ORM\Column(name="value")
     */
    private $value;
    /**
     * @ORM\Column(name="img")
     */
    private $img;
    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\UserQuestionAnswer", mappedBy="answer")
     * @ORM\JoinColumn(name="id", referencedColumnName="answer_id")
     */
    protected $userQuestionAnswers;

    public function __construct()
    {
        $this->userQuestionAnswers = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return UserQuestionAnswer[]
     */
    public function getUserQuestionAnswers()
    {
        $criteria = Criteria::create();

        return $this->userQuestionAnswers->matching($criteria);
    }

    public function addUserQuestionAnswer(UserQuestionAnswer $userQuestionAnswer)
    {
        $this->userQuestionAnswers[] = $userQuestionAnswer;
    }

    /**
     * @return Question
     */
    public function getQuestion(): Question
    {
        return $this->question;
    }

    /**
     * @param Question $question
     */
    public function setQuestion(Question $question): void
    {
        $this->question = $question;
        $question->addAnswer($this);
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state): void
    {
        $this->state = $state;
    }

    public function isTruth()
    {
        return $this->type === self::TYPE_VARIANT ? $this->point === self::STATUS_TRUTH : true;
    }

    /**
     * @return mixed
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * @param mixed $point
     */
    public function setPoint($point): void
    {
        $this->point = $point;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     */
    public function setImg($img): void
    {
        $this->img = $img;
    }

    public function validateParentQuestion(Question $question)
    {
        if ($this->getQuestion()->getId() !== $question->getId()) {
            throw new Exception('Question id ' . $question->getId() . ' is not parent for answer id ' . $this->getId());
        }
    }

    public function exchangeArray(array $data)
    {
        if (isset($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['state'])) {
            $this->state = $data['state'];
        }
        if (isset($data['weight'])) {
            $this->weight = $data['weight'];
        }
        if (isset($data['value'])) {
            $this->value = $data['value'];
        }
        if (isset($data['point'])) {
            $this->point = $data['point'];
        }
        if (isset($data['img'])) {
            $this->img = $data['img'];
        }
    }

    /**
     * Для валидации загруженой формы
     */
    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'question_id' => $this->question->getId(),
            'state' => $this->state,
            'weight' => $this->weight,
            'value' => $this->value,
            'point' => $this->point,
            'img' => $this->img,
        ];
    }
}
