<?php

namespace Survey\Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\Survey\Core\Repository\TestingRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="testing")
 */
class Testing
{
    use TimestampableEntityTraid;

    const STATE_DELETE = 0; // удаление программы оценивания
    const STATE_ACTIVE = 1; // активная система оценивания
    const STATE_STOP = 2; // приостановленая система оценивания

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(name="description")
     */
    private $description; // описание программы
    /**
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\User", inversedBy="testing")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user; // автор создания программы оценивания
    /**
     * @ORM\Column(name="project_id")
     */
    private $projectId; // ключ проекта
    /**
     * @ORM\Column(name="state", type="integer")
     */
    private $state; // состояние программы тестирования (активная/удаленная)
    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\TestingTest", mappedBy="testing")
     * @ORM\JoinColumn(name="id", referencedColumnName="testing_id")
     */
    private $testingTests;

    public function __construct()
    {
        $this->testingTests = new ArrayCollection();
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
        $user->addTesting($this);
    }

    public function getTestingTests($isActiveTestingTest = true)
    {
        $criteria = Criteria::create();
        if ($isActiveTestingTest) {
            $criteria->where(Criteria::expr()->neq('state', TestingTest::STATE_DELETE));
        }
        return $this->testingTests->matching($criteria);
    }

    public function getTestingTestsThanCurrent(TestingTest $testingTest)
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->neq('state', TestingTest::STATE_DELETE));
        $criteria->andWhere(Criteria::expr()->notIn('id', [$testingTest]));

        return $this->testingTests->matching($criteria);
    }
    
    public function addTestingTest(TestingTest $testingTest): void
    {
        $this->testingTests[] = $testingTest;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * @param mixed $projectId
     */
    public function setProjectId($projectId): void
    {
        $this->projectId = $projectId;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    public function isActive()
    {
        return ($this->state !== self::STATE_DELETE) ? true : false;
    }

    /**
     * @param mixed $state
     */
    public function setState($state): void
    {
        $this->state = $state;
    }

    public function exchangeArray(array $data)
    {
        if (isset($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['description'])) {
            $this->description = $data['description'];
        }
        if (isset($data['project_id'])) {
            $this->projectId = $data['project_id'];
        }
        if (isset($data['state'])) {
            $this->state = $data['state'];
        }
    }

    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'user' => $this->user,
            'project_id' => $this->projectId,
            'state' => $this->state,
        ];
    }
}
