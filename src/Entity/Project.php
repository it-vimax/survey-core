<?php

namespace Survey\Core\Entity;

class Project
{
    const PROJECT_VOLIA_HOME = 1; // тесты для воля.хом
    const PROJECT_EASY_LEARN = 2; // тесты для базы знаний
    const PROJECT_HR = 3; // тесты для HR

    public static $allProjects = [
        self::PROJECT_VOLIA_HOME => 'Портал Воля Хом',
        self::PROJECT_EASY_LEARN => 'База Знаний',
        self::PROJECT_HR => 'HR',
    ];

    public static function getProjectList($projectIds)
    {
        $allProjectsFlip = array_flip(self::$allProjects);
        $allProjectsExistsFlip = array_intersect($allProjectsFlip, $projectIds);
        $allProjectsExists = array_flip($allProjectsExistsFlip);

        return $allProjectsExists;
    }

    public static function getProjectNameById($id)
    {
        $list = self::getProjectList(array_flip(self::$allProjects));

        return $list[$id];
    }
}
