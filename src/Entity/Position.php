<?php

namespace Survey\Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\Survey\Core\Repository\PositionRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="positions")
 */
class Position
{
    use TimestampableEntityTraid;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(name="name")
     */
    protected $name;
    /**
     * @ORM\Column(name="is_supervisor", type="integer")
     */
    private $isSupervisor;

    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\User", mappedBy="position")
     * @ORM\JoinColumn(name="id", referencedColumnName="position_id")
     */
    protected $users;
    /**
     * @ORM\Column(name="global_id")
     */
    private $globalId;
    /**
     * @ORM\ManyToMany(targetEntity="\Survey\Core\Entity\Department", mappedBy="positions")
     * @ORM\JoinTable(name="departments_positions",
     *      joinColumns={@ORM\JoinColumn(name="position_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="department_id", referencedColumnName="id")}
     *      )
     */
    private $departments;

    
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->departments = new ArrayCollection();
    }

    public function getGlobalId()
    {
        return $this->globalId;
    }

    public function setGlobalId($globalId)
    {
        $this->globalId = $globalId;
    }

    /**
     * @return User[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return User[]
     */
    public function getUsersByDepartment(Department $department)
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('department', $department));
        $criteria->andWhere(Criteria::expr()->eq('status', User::STATUS_ACTIVE));

        return $this->users->matching($criteria);
    }

    /**
     * @param mixed $user
     */
    public function addUser(User $user): void
    {
        $this->users[] = $user;
    }

    public function removeElement(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function isSupervisor()
    {
        return $this->isSupervisor === 1 ? true : false;
    }

    public function setIsSupervisor($isSupervisor)
    {
        $this->isSupervisor = $isSupervisor;
    }

    /**
     * @return Department[]
     */
    public function getDepartments()
    {
        return $this->departments;
    }

    public function addDepartment(Department $department)
    {
        $this->departments[] = $department;
    }

    public function removeDepartment(Department $department)
    {
        $this->departments->removeElement($department);
    }

    public function exchangeArray(array $data)
    {
        if (isset($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['global_id'])) {
            $this->globalId = $data['global_id'];
        }
        if (isset($data['name'])) {
            $this->name = $data['name'];
        }
        if (isset($data['is_supervisor'])) {
            $this->isSupervisor = $data['is_supervisor'];
        }
    }

    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'global_id' => $this->globalId,
            'name' => $this->name,
            'is_supervisor' => $this->isSupervisor,
        ];
    }
}
