<?php

namespace Survey\Core\Entity;

use DateTime;
use DateInterval;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Laminas\Validator\Date;

/**
 * @ORM\Entity(repositoryClass="\Survey\Core\Repository\TestingTestRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="testing_tests")
 */
class TestingTest
{
    use TimestampableEntityTraid;

    const STATE_DELETE = 0; // удален тест с программы тестирования
    const STATE_ACTIVE = 1; // тест активный в программе тестирования
    const STATE_DRAFT = 2; // тест уже создан и стоит в режиме добавления/удаления пользователей
    const STATE_STOP = 3; // тест остановлен

    // статусы теста в зависимости от временного промежутка
    const STATUS_EXPECTS = 1; // тест ожидает начала
    const STATUS_IN_WORK = 2; // тест начался
    const STATUS_COMPLETED = 3; // тест закончился
    const STATUS_ERROR = 4; // тест в непонятном статусе так, как дата начала и окончания не валидна

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var Testing
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\Testing", inversedBy="testingTests")
     * @ORM\JoinColumn(name="testing_id", referencedColumnName="id")
     */
    protected $testing;
    /**
     * @var Test
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\Test", inversedBy="testingTests")
     * @ORM\JoinColumn(name="test_id", referencedColumnName="id")
     */
    protected $test;
    /**
     * @ORM\OneToMany(targetEntity="\Survey\Core\Entity\UserQuestionAnswer", mappedBy="testingTest")
     * @ORM\JoinColumn(name="id", referencedColumnName="testing_test_id")
     */
    protected $userQuestionAnswers;
    /**
     * @ORM\Column(name="state", type="integer")
     */
    protected $state;
    /**
     * @ORM\Column(name="weight", type="integer")
     */
    protected $weight;
    /**
     * @ORM\Column(name="is_cycle_control", type="integer")
     */
    protected $isCycleControl;
    /**
     * @ORM\Column(name="is_notify_incorrect_answer", type="integer")
     */
    protected $isNotifyIncorrectAnswer;
    /**
     * @var TestingTest
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\MessageTemplate", inversedBy="notifyIncorrectTestSupervisorTestingTests")
     * @ORM\JoinColumn(name="notify_incorrect_test_supervisor_id", referencedColumnName="id")
     */
    protected $notifyIncorrectTestBySupervisor;
    /**
     * @var TestingTest
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\MessageTemplate", inversedBy="notifyIncorrectTestUserTestingTests")
     * @ORM\JoinColumn(name="notify_incorrect_test_user_id", referencedColumnName="id")
     */
    protected $notifyIncorrectTestByUser;
    /**
     * @ORM\Column(name="description")
     */
    protected $description;
    /**
     * @var DateTime
     * @ORM\Column(name="start_test_at", type="datetime")
     */
    protected $startTestAt;
    /**
     * @var DateTime
     * @ORM\Column(name="finish_test_at", type="datetime")
     */
    protected $finishTestAt;

    /**
     * @ORM\Column(name="is_notify_start", type="integer")
     */
    protected $isNotifyStart;
    /**
     * @var MessageTemplate
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\MessageTemplate", inversedBy="notifyStartTestingTests")
     * @ORM\JoinColumn(name="notify_start_id", referencedColumnName="id")
     */
    protected $notifyStart;
    /**
     * @ORM\Column(name="is_notify_reminder", type="integer")
     */
    protected $isNotifyReminder;
    /**
     * @ORM\Column(name="notify_reminder_days", type="integer")
     */
    protected $notifyReminderDays;
    /**
     * @var TestingTest
     * @ORM\ManyToOne(targetEntity="\Survey\Core\Entity\MessageTemplate", inversedBy="notifyReminderTestingTests")
     * @ORM\JoinColumn(name="notify_reminder_id", referencedColumnName="id")
     */
    protected $notifyReminder;

    /**
     * @ORM\ManyToMany(targetEntity="\Survey\Core\Entity\User", mappedBy="testingTests")
     */
    protected $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->userQuestionAnswers = new ArrayCollection();
    }

    /**
     * @return UserQuestionAnswer[]
     */
    public function getUserQuestionAnswers()
    {
        return $this->userQuestionAnswers;
    }

    public function getUserQuestionAnswersByUser(User $user)
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('user', $user));

        return $this->userQuestionAnswers->matching($criteria)->toArray();
    }

    public function addUserQuestionAnswer(UserQuestionAnswer $userQuestionAnswer)
    {
        $this->userQuestionAnswers[] = $userQuestionAnswer;
    }

    public function isNotifyIncorrectAnswer()
    {
        return $this->isNotifyIncorrectAnswer === 0 ? false : true;
    }

    public function setNotifyIncorrectAnswer($value)
    {
        return $this->isNotifyIncorrectAnswer = $value;
    }

    public function getNotifyIncorrectTestBySupervisor(): ?MessageTemplate
    {
        return $this->notifyIncorrectTestBySupervisor;
    }

    public function setNotifyIncorrectTestBySupervisor(?MessageTemplate $template)
    {
        if (empty($template)) {
            if (!empty($this->getNotifyIncorrectTestBySupervisor())) {
                $this->getNotifyIncorrectTestBySupervisor()
                    ->removeNotifyIncorrectTestSupervisorElement($this);
            }
        } else {
            $template->addNotifyIncorrectTestSupervisorTestingTests($this);
        }
        $this->notifyIncorrectTestBySupervisor = $template;
    }

    public function getNotifyIncorrectTestByUser(): ?MessageTemplate
    {
        return $this->notifyIncorrectTestByUser;
    }

    public function setNotifyIncorrectTestByUser(?MessageTemplate $template)
    {
        if (empty($template)) {
            if (!empty($this->getNotifyIncorrectTestByUser())) {
                $this->getNotifyIncorrectTestByUser()
                    ->removeNotifyIncorrectTestUserElement($this);
            }
        } else {
            $template->addNotifyIncorrectTestUserTestingTests($this);
        }
        $this->notifyIncorrectTestByUser = $template;
    }

    /**
     * @return User[]
     */
    public function getUsers()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('status', User::STATUS_ACTIVE));

        return $this->users->matching($criteria);
    }

    /**
     * @return User[]
     */
    public function getUsersPassedTest(TestingTest $testingTest)
    {
        $users = [];

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('status', User::STATUS_ACTIVE));

        $allUsers = $this->users->matching($criteria);
        if (count($allUsers) > 0) {
            /** @var User $one */
            foreach ($allUsers as $one) {
                if ($one->isPassedTestingTest($testingTest)) {
                    $users[] = $one;
                }
            }
        }

        return $users;
    }
    /**
     * @return User[]
     */
    public function getUsersNotPassTest(TestingTest $testingTest)
    {
        $users = [];

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('status', User::STATUS_ACTIVE));

        $allUsers = $this->users->matching($criteria);
        if (count($allUsers) > 0) {
            /** @var User $one */
            foreach ($allUsers as $one) {
                if (!$one->isPassedTestingTest($testingTest)) {
                    $users[] = $one;
                }
            }
        }

        return $users;
    }
    
    /**
     * @return boolean
     */
    public function isUserExist(User $user)
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('status', User::STATUS_ACTIVE));
        $criteria->andWhere(Criteria::expr()->eq('id', $user->getId()));
        
        return count($this->users->matching($criteria)) > 0 ? true : false;
    }

    public function addUser(User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    public function removeUserAssociation(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Test
     */
    public function getTest(): Test
    {
        return $this->test;
    }

    /**
     * @param mixed $test
     */
    public function setTest(Test $test)
    {
        $this->test = $test;
        $test->addTestingTest($this);
    }

    public function getNotifyStartMessageTemplate(): ?MessageTemplate
    {
        return $this->notifyStart;
    }

    public function setNotifyStartMessageTemplate(?MessageTemplate $notifyStart)
    {
        if (empty($template)) {
            if (!empty($this->getNotifyStartMessageTemplate())) {
                $this->getNotifyStartMessageTemplate()
                    ->removeNotifyStartElement($this);
            }
        } else {
            $template->addNotifyStartTestingTest($this);
        }
        $this->notifyStart = $notifyStart;
    }

    public function getNotifyReminderMessageTemplate(): ?MessageTemplate
    {
        return $this->notifyReminder;
    }

    public function setNotifyReminderMessageTemplate(?MessageTemplate $template)
    {
        if (empty($template)) {
            if (!empty($this->getNotifyReminderMessageTemplate())) {
                $this->getNotifyReminderMessageTemplate()
                    ->removeNotifyReminderElement($this);
            }
        } else {
            $template->addNotifyReminderTestingTest($this);
        }
        $this->notifyReminder = $template;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return mixed $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return Testing
     */
    public function getTesting(): Testing
    {
        return $this->testing;
    }

    /**
     * @param mixed $testing
     */
    public function setTesting(Testing $testing): void
    {
        $this->testing = $testing;
        $testing->addTestingTest($this);
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state): void
    {
        $this->state = $state;
    }

    /**
     * @return DateTime
     */
    public function getStartTestAt()
    {
        return $this->startTestAt;
    }

    /**
     * @param mixed $startTestAt
     */
    public function setStartTestAt($startTestAt): void
    {
        $this->startTestAt = $startTestAt;
    }

    /**
     * @return DateTime
     */
    public function getFinishTestAt()
    {
        return $this->finishTestAt;
    }

    /**
     * @param mixed $finishTestAt
     */
    public function setFinishTestAt($finishTestAt): void
    {
        $this->finishTestAt = $finishTestAt;
    }

    public function isDraft()
    {
        if ($this->state === self::STATE_DRAFT) {
            return true;
        }
        return false;
    }

    public function isDelete()
    {
        return ($this->state === self::STATE_DELETE) ? true : false;
    }

    public function isStopped()
    {
        return $this->state === self::STATE_STOP ? true : false;
    }
    
    public function isNotifyStart()
    {
        return $this->isNotifyStart === 0 ? false : true;
    }

    public function isCanNotifyRemind()
    {
        $triger = $this->isNotifyReminder === 0 ? false : true;
        $days = $this->notifyReminderDays;
        $finishDate = $this->finishTestAt;
        $date = new DateTime(date('Y-m-d'));
        $date->add(new DateInterval('P' . $days . 'D'));

        if ($triger && $date == $finishDate) {
            return true;
        }

        return false;
    }
    public function isNotifyRemind()
    {
        return $this->isNotifyReminder === 0 ? false : true;
    }

    public function isCycleControl(): bool
    {
        return $this->isCycleControl === 0 ? false : true;
    }


    public function validateParentTesting(Testing $testing)
    {
        if ($this->testing->getId() !== $testing->getId()) {
            throw new Exception('The testing ID ' . $testing->getId() . ' is not parent by the testing test ID ' . $this->id);
        }
    }

    public function getTestStatus()
    {
        $now = new DateTime(date('Y-m-d'));

        if ($this->startTestAt > $now && $this->finishTestAt > $now) {
            return self::STATUS_EXPECTS;
        } elseif ($this->startTestAt <= $now && $this->finishTestAt >= $now) {
            return self::STATUS_IN_WORK;
        } elseif ($this->startTestAt < $now && $this->finishTestAt < $now) {
            return self::STATUS_COMPLETED;
        } else {
            return self::STATUS_ERROR;
        }
    }

    public function isValidateDate()
    {
        $currentData = new DateTime();

        if ($currentData > $this->startTestAt && $currentData < $this->finishTestAt)
            return true;

        return false;
    }

    public function exchangeArray(array $data)
    {
        if (isset($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['state'])) {
            $this->state = $data['state'];
        }
        if (isset($data['start_test_at'])) {
            $datetime = new DateTime($data['start_test_at']);
            $this->startTestAt = $datetime;
        }
        if (isset($data['finish_test_at'])) {
            $datetime = new DateTime($data['finish_test_at']);
            $this->finishTestAt = $datetime;
        }
        if (isset($data['description'])) {
            $this->description = $data['description'];
        }
        if (isset($data['weight'])) {
            $this->weight = $data['weight'];
        }
        if (isset($data['is_cycle_control'])) {
            $this->isCycleControl = $data['is_cycle_control'];
        }
        if (isset($data['is_notify_incorrect_answer'])) {
            $this->isNotifyIncorrectAnswer = $data['is_notify_incorrect_answer'];
        }
        if (isset($data['is_notify_start'])) {
            $this->isNotifyStart = $data['is_notify_start'];
        }
        if (isset($data['is_notify_reminder'])) {
            $this->isNotifyReminder = $data['is_notify_reminder'];
        }
        if (isset($data['notify_reminder_days'])) {
            $this->notifyReminderDays = $data['notify_reminder_days'];
        }
    }

    public function getArrayCopy()
    {
        return [
            'id' => $this->id,
            'testing_id' => $this->testing->getId(),
            'test_id' => $this->test->getId(),
            'state' => $this->state,
            'weight' => $this->weight,
            'description' => $this->description,
            'start_test_at' => $this->startTestAt->format('Y-m-d'),
            'finish_test_at' => $this->finishTestAt->format('Y-m-d'),
            'is_notify_incorrect_answer' => $this->isNotifyIncorrectAnswer,
            'is_cycle_control' => $this->isCycleControl,
            'notify_incorrect_test_supervisor_id' => !empty($this->notifyIncorrectTestBySupervisor) ? $this->notifyIncorrectTestBySupervisor->getId() : null,
            'notify_incorrect_test_user_id' => !empty($this->notifyIncorrectTestByUser) ? $this->notifyIncorrectTestByUser->getId() : null,
            'is_notify_start' => $this->isNotifyStart,
            'notify_start_id' => !empty($this->notifyStart) ? $this->notifyStart->getId() : null,
            'is_notify_reminder' => $this->isNotifyReminder,
            'notify_reminder_days' => $this->notifyReminderDays,
            'notify_reminder_id' => !empty($this->notifyReminder) ? $this->notifyReminder->getId() : null,
        ];
    }

    public static function getReportKeysInfo(TestingTest $testingTest)
    {
        $baseKeys = [
            '{testing_description} => Название программы тестирования',
            '{test_title} => Название теста',
            '{testing_test_description} => Описание теста  в  програме тестирования',
            '{test_type} => Тип теста',
            '{testing_test_create_at} => Время добавление теста к программе тестирования',
            '{testing_test_start_at} => Время старта теста',
            '{testing_test_finish_at} => Время окончания теста',
            '{current_date} => Дата формирования отчёта',

            '{total_users} => Количество сотрудников, участвующих в тесте',
            '{quantity_passed_test} => Количество сотрудников, которые прошли тест',
            '{quantity_must_passed_test} => Количество сотрудников, которые еще не прошли тест',
            '{user_finish_test_percent} => Процент сотрудников, что прошли тест (%)',

            '{total_question_in_test} => Общее количество вопросов в тесте',

            '[users_id] => ID',
            '[users_name] => Сотрудник',
            '[users_email] => Почта',
            '[users_test_create_at] => Дата прохождения',
            '[users_truth_percent] => Процент положит.  ответов (%)',
            '{average_result_in_percent} => Средний результат прохождения теста сотрудниками (%)',
        ];

        $customKeys = [];
        switch ($testingTest->getTest()->getType()) {
            case Test::TYPE_RATING:
                $customKeys = [
                    '{total_ball_in_rating} => Общее количество баллов в опроснике',
                    '[users_quantity_balls] => Кол. Баллов набр.сотруд.',
                ];
                break;

            case Test::TYPE_TEST:
                $customKeys = [
                    '{total_truth_answer_in_test} => Количество правильных ответов в тесте',
                    '[users_truth_answer] => Количество правильных ответов в тесте',
                ];
        }

        return array_merge($baseKeys, $customKeys);
    }
}
