<?php

namespace Survey\Core\Utils;

use Doctrine\ORM\EntityManager;
use Survey\Core\Entity\TestingTest;
use Survey\Core\Entity\User;
use Survey\Core\Entity\UserQuestionAnswer;
use Survey\Core\Helper\AccessHelper;
use Survey\Core\Helper\TestResultEmailRender;

class MessageShortCode
{
    const USER_NAME = 'USER_NAME';
    const EMPLOYEE_NAME = 'EMPLOYEE_NAME';
    const TEST_NAME = 'TEST_NAME';
    const TEST_LINK = 'TEST_LINK';
    const TEST_DATE_START = 'TEST_DATE_START';
    const TEST_DATE_FINISH = 'TEST_DATE_FINISH';
    const TEST_RESULT_INFO = 'TEST_RESULT_INFO';
    const EMPLOYEE_TEST_RESULT_INFO = 'EMPLOYEE_TEST_RESULT_INFO';

    private $shortCodes = [];
    private $entityManager;
    private $testingTest;
    private $user;
    private $employee;
    private $urlServer;

    public function __construct(
        EntityManager $entityManager,
        ?User $user,
        ?TestingTest $testingTest,
        $urlServer,
        ?User $employee = null
    ) {
        $this->entityManager = $entityManager;
        $this->user = $user;
        $this->employee = $employee;
        $this->testingTest = $testingTest;
        $this->urlServer = $urlServer;

        $this->init();
    }

    public function formattingTemplate($template)
    {
        return preg_replace_callback(
            "@(\&lt;|<)%(.+?)\%(&gt;|>)@",
            function($matches) {
                return isset($this->shortCodes[$matches[2]]) ? $this->shortCodes[$matches[2]] : "{$matches[0]}";
            },
            $template
        );
    }

    private function init()
    {
        if (!empty($this->user)) {
            $this->shortCodes[self::USER_NAME] = $this->user->getFullName();
        }
        
        if (!empty($this->testingTest)) {
            $this->shortCodes[self::TEST_DATE_START] = $this->testingTest->getStartTestAt()->format('d/m/Y');
            $this->shortCodes[self::TEST_DATE_FINISH] = $this->testingTest->getFinishTestAt()->format('d/m/Y');
            $this->shortCodes[self::TEST_NAME] = $this->testingTest->getTest()->getTitle();
        }
        
        if (!empty($this->user) && !empty($this->testingTest)) {
            $this->shortCodes[self::TEST_LINK] = $this->generateTestingTestLink();

            $userQuestionAnswers = $this->entityManager
                ->getRepository(UserQuestionAnswer::class)
                ->findWithMaxAttempt($this->user, $this->testingTest);
            if (!empty($userQuestionAnswers)) {
                $this->shortCodes[self::TEST_RESULT_INFO] = $this->generateTestingTestResultInfo($userQuestionAnswers);
            }
        }

        if (!empty($this->employee)) {
            $this->shortCodes[self::EMPLOYEE_NAME] = $this->employee->getFullName();
        }

        if (!empty($this->employee) && !empty($this->testingTest)) {
            $employeeQuestionAnswers = $this->entityManager
                ->getRepository(UserQuestionAnswer::class)
                ->findWithMaxAttempt($this->employee, $this->testingTest);
            if (!empty($employeeQuestionAnswers)) {
                $this->shortCodes[self::EMPLOYEE_TEST_RESULT_INFO] = $this->generateTestingTestResultInfo($employeeQuestionAnswers);
            }
        }
    }

    private function generateTestingTestResultInfo($userQuestionAnswers)
    {
        $result = '';

        if (!empty($userQuestionAnswers)) {
            /** @var UserQuestionAnswer $firstAnswer */
            $firstAnswer = reset($userQuestionAnswers);
            $result .= TestResultEmailRender::renderTitle($firstAnswer);
            $result .= TestResultEmailRender::renderTestLog($this->entityManager, $userQuestionAnswers);
        }

        return $result;
    }

    private function generateTestingTestLink()
    {
        $testLink = '<a target="_blank" href="' . $this->urlServer . '/?token='
                        . AccessHelper::encodeTestToken(
                                $this->testingTest->getId(),
                                $this->user->getLastTestingTestAttempt($this->testingTest),
                                $this->user->getEmail()
                            )
                        .'">' . $this->testingTest->getTest()->getTitle() . '</a>';

        return $testLink;
    }
}
