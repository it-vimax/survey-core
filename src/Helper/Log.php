<?php

namespace Survey\Core\Helper;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Log
{
    private $isRequestResponseLogging;

    public function __construct($config)
    {
        $this->isRequestResponseLogging = $config['is_request_response_logging'] ?? false;
    }

    public function requestLog($context, $responseDate = "")
    {
        if ($this->isRequestResponseLogging) {
            $path = './data/request-log/' . date('Y') . '/' . date('m') . '/send-message_' . date('Y-m-d') . '.log';

            $requestUri = $context->getRequest()->getRequestUri();
            $post = $context->getRequest()->getPost();

            $message = "|Action Name: " . debug_backtrace()[1]['function']
                . "|URI: $requestUri"
                . "|POST: " . json_encode($post)
                . "|Response: " . json_encode($responseDate);

            $log = new Logger('Module = API');
            $log->pushHandler(new StreamHandler($path, Logger::INFO));
            $log->info($message);
        }
    }

    public static function error($exceptionName, $file, $line, $stackTrace, $errorMessage = null, $controllerName = null)
    {
        $path = './data/log/' . date('Y') . '/' . date('m') . '/error_' . date('Y-m-d') . '.log';

        $log = new Logger('Module = Admin');
        $log->pushHandler(new StreamHandler($path, Logger::WARNING));
        $log->error(
            'exceptionName => ' . $exceptionName . "|" .
            'file => ' . $file . "|" .
            'line => ' . $line . "|" .
            'stackTrace => ' . $stackTrace . "|" .
            'errorMessage => ' . $errorMessage . "|" .
            'controllerName => ' . $controllerName
        );
    }

    public static function userImport($message, $isError = false)
    {
        $path = './data/user-import-log/' . date('Y') . '/' . date('m') . '/user-import_' . date('Y-m-d') . '.log';
        
        $log = new Logger('Module = User');
        if ($isError === false) {
            $log->pushHandler(new StreamHandler($path, Logger::INFO));
            $log->info($message);
        } else {
            $log->pushHandler(new StreamHandler($path, Logger::ERROR));
            $log->error($message);
        }
    }

    public static function sendMessageLog($email, $format, $subject, $body)
    {
        $path = './data/send-message-log/' . date('Y') . '/' . date('m') . '/send-message_' . date('Y-m-d') . '.log';

        $message = "Email: $email"
            . "|Message format: $format"
            . "|Subject: $subject"
            . "|Body: $body";

        $log = new Logger('Module = Console');
        $log->pushHandler(new StreamHandler($path, Logger::INFO));
        $log->info($message);
    }
}
