<?php

namespace Survey\Core\Helper;

use Doctrine\ORM\EntityManager;
use Survey\Core\Entity\Answer;
use Survey\Core\Entity\City;
use Survey\Core\Entity\Position;
use Survey\Core\Entity\SubdivisionCity;
use Survey\Core\Entity\UserFunction;
use Survey\Core\Entity\UserQuestionAnswer;

class TestResultEmailRender
{
    public static function renderTitle(UserQuestionAnswer $userQuestionAnswer)
    {
        /** @var SubdivisionCity $subdivision */
        $subdivision =  $userQuestionAnswer->getUser()->getSubdivisionCity();
        /** @var City $city */
        $city = null;
        if (!empty($subdivision)) {
            $city = $subdivision->getCity();
        }
        /** @var Position $position */
        $position = $userQuestionAnswer->getUser()->getPosition();
        /** @var UserFunction $userFunction */
        $userFunction = $userQuestionAnswer->getUser()->getUserFunction();

        $html = '<div>';
        $html .= '<p>Лог прохождения тестирования пользователем</p>';
        $html .= '<p>Пользователь: ' . $userQuestionAnswer->getUser()->getEmail() . '</p>';
        $html .= '<p>Подразделение: ' . (isset($subdivision) ? $subdivision->getName() : "не указано") . (isset($city) ? '(' . $city->getName() . ')' : "") . '</p>';
        $html .= '<p>Должность: ' . (isset($position) ? $position->getName() : "не указано") . (isset($userFunction) ? '(' .  $userFunction->getName() . ')' : "") . '</p>';
        $html .= '<p>Тест: ' . $userQuestionAnswer->getTestingTest()->getTest()->getTitle() . '</p>';
        $html .= '<p>Дата начала/завершения: ' . $userQuestionAnswer->getTestingTest()->getStartTestAt()->format('d-m-Y')
            . '/' .  $userQuestionAnswer->getTestingTest()->getFinishTestAt()->format('d-m-Y') . '</p>';
        $html .= '<p>Дата прохождения: ' . $userQuestionAnswer->getCreateAt()->format('d-m-Y H:s:i') . '</p>';
        $html .= '<p>Номер попытки: ' . $userQuestionAnswer->getAttempt() . '</p>';
        $html .= '<div>';

        return $html;
    }

    public static function renderTestLog(EntityManager $em, $userQuestionAnswers)
    {
        $html = '';

        if (!empty($userQuestionAnswers)) {
            $html = '<table>';
            $html .= '<tr>';
            $html .= '<td style="border: #000000 solid 2px; font-weight: 600;">';
            $html .= '№';
            $html .= '</td>';
            $html .= '<td style="border: #000000 solid 2px; font-weight: 600;">';
            $html .= 'Вопрос';
            $html .= '</td>';
            $html .= '<td style="border: #000000 solid 2px; font-weight: 600;">';
            $html .= 'Правильный ответ';
            $html .= '</td>';
            $html .= '<td style="border: #000000 solid 2px; font-weight: 600;">';
            $html .= 'Ответ пользователя';
            $html .= '</td>';
            $html .= '<td style="border: #000000 solid 2px; font-weight: 600;">';
            $html .= 'Результат';
            $html .= '</td>';
            $html .= '</tr>';

            /** @var UserQuestionAnswer $userQuestionAnswer */
            foreach ($userQuestionAnswers as $number => $userQuestionAnswer) {
                $html .= '<tr style="';
                !$userQuestionAnswer->getAnswer()->isTruth() ? $html .= ' background: #FFFC2E;' : null;
                $html .= '">';
                $html .= '<td style="border: #000000 dashed 1px;">';
                $html .= $number + 1;
                $html .= '</td>';
                $html .= '<td style="border: #000000 dashed 1px;">';
                $html .= $userQuestionAnswer->getQuestion()->getValue();
                $html .= '</td>';
                $html .= '<td style="border: #000000 dashed 1px;">';
                $html .= self::setAllTruthAnswers($em, $userQuestionAnswer->getQuestion()->getId());
                $html .= '</td>';
                $html .= '<td style="border: #000000 dashed 1px;">';
                $html .= $userQuestionAnswer->getAnswer()->getValue();
                $html .= '</td>';
                $html .= '<td style="border: #000000 dashed 1px;">';
                $html .= $userQuestionAnswer->getAnswer()->isTruth() ? 'Верно' : 'Ошибка';
                $html .= '</td>';
                $html .= '</tr>';
            }

            $html .= '</table>';
        }

        return $html;
    }

    private static function setAllTruthAnswers(EntityManager $em, $questionId)
    {
        $txt = '';

        /** @var Answer[] $allTruthAnswers */
        $allTruthAnswers = $em->getRepository(Answer::class)
            ->findAllActiveTruthAnswerOfQuestion($questionId);

        if (!empty($allTruthAnswers)) {
            $isFirstTruthAnswer = true;
            foreach ($allTruthAnswers as $key => $allTruthAnswer) {
                $txt .= !$isFirstTruthAnswer ? ',<br>' : '';
                $txt .= ($key + 1) . ") ";
                $txt .= "\"";
                $txt .= $allTruthAnswer->getValue();
                $txt .= "\"";
                $isFirstTruthAnswer = false;
            }
        }

        return $txt;
    }
}
