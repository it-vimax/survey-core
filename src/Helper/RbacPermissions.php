<?php

namespace Survey\Core\Helper;

class RbacPermissions
{
    const PERMISSION_VIEW = 'permission.view';
    const PERMISSION_INDEX = 'permission.index';
    const PERMISSION_EDIT = 'permission.edit';
    const PERMISSION_DELETE = 'permission.delete';
    const PERMISSION_CREATE = 'permission.create';

    const ROLE_VIEW = 'role.view';
    const ROLE_INDEX = 'role.index';
    const ROLE_EDIT = 'role.edit';
    const ROLE_EDIT_PERMISSIONS = 'role.edit-permissions';
    const ROLE_DELETE = 'role.delete';
    const ROLE_CREATE = 'role.create';

    const USER_VIEW = 'user.view';
    const USER_INDEX = 'user.index';
    const USER_EDIT = 'user.edit';
    const USER_CREATE = 'user.create';

    const TEST_VIEW = 'test.view';
    const TEST_INDEX = 'test.index';
    const TEST_EDIT = 'test.edit';
    const TEST_DELETE = 'test.delete';
    const TEST_CREATE = 'test.create';

    const PROJECT_EASY_LEARN_VIEW = 'project.easy-learn.view';

    const PROJECT_VOLIA_HOME_VIEW = 'project.volia-home.view';

    const PROJECT_HR_VIEW = 'project.hr.view';

    const PROJECT_ACCESS = 'project.access';

    const TESTING_VIEW = 'testing.view';
    const TESTING_INDEX = 'testing.index';
    const TESTING_EDIT = 'testing.edit';
    const TESTING_DELETE = 'testing.delete';
    const TESTING_CREATE = 'testing.create';

    const TESTING_TEST_VIEW = 'testing-test.view';
    const TESTING_TEST_DELETE = 'testing-test.delete';
    const TESTING_TEST_CREATE = 'testing-test.create';
    const TESTING_TEST_DRAFT = 'testing-test.draft';
    const TESTING_TEST_GENERATE_EXCEL_REPORT = 'testing-test.generate-excel-report';
    const TESTING_TEST_ATTACH_USER = 'testing-test.attach-user';
    const TESTING_TEST_DETACH_USER = 'testing-test.detach-user';
    const TESTING_TEST_RUN = 'testing-test.run';

    const ANSWER_IMAGE_UPLOAD = 'answer.image-upload';
    const ANSWER_DELETE = 'answer.delete';
    const ANSWER_CREATE = 'answer.create';

    const QUESTION_VIEW = 'question.view';
    const QUESTION_IMAGE_UPLOAD = 'question.image-upload';
    const QUESTION_DELETE = 'question.delete';
    const QUESTION_CREATE = 'question.create';

    const USER_QUESTION_ANSWER_SHOW_INFORMATION = 'user-question-answer.show-information';

    const TEST_PERSON_VIEW = 'test-person.view';

    const MESSAGE_TEMPLATE_VIEW = 'email-template.view';
    const MESSAGE_TEMPLATE_INDEX = 'email-template.index';
    const MESSAGE_TEMPLATE_EDIT = 'email-template.edit';
    const MESSAGE_TEMPLATE_DELETE = 'email-template.delete';
    const MESSAGE_TEMPLATE_CREATE = 'email-template.create';
}
