<?php

namespace Survey\Core\Helper;

use Exception;

class AccessHelper
{
    const SOLE = "6F^D(ffsf9FD@!&&";
    const CIPHER = "AES-128-CBC";

    private static function encodeToken(array $data)
    {
        $dataStr = serialize($data);

        $ciphertext_raw = openssl_encrypt($dataStr, self::CIPHER, self::SOLE, 0, self::SOLE);

        return base64_encode($ciphertext_raw);
    }

    private static function decodeToken($tokenBase64)
    {
        if (empty($tokenBase64)) {
            throw new Exception('Empty token!');
        }

        $token = base64_decode($tokenBase64);
        $encodeDataStr = openssl_decrypt($token, self::CIPHER, self::SOLE, 0, self::SOLE);

        if (empty($encodeDataStr)) {
            throw new Exception('Failed decrypt token!');
        }

        return unserialize($encodeDataStr);
    }

    public static function encodeTestToken($testingTestId, $attempt, $userEmail)
    {
        $data = [
            'testingTestId' => $testingTestId,
            'userEmail' => $userEmail,
            'attempt' => $attempt,
        ];

        return self::encodeToken($data);
    }

    public static function decodeTestToken($tokenBase64)
    {
        $dataArr = self::decodeToken($tokenBase64);

        if (empty($dataArr) ||
            empty($dataArr['testingTestId'])  ||
            empty($dataArr['userEmail']) ||
            !isset($dataArr['attempt'])
        ) {
            throw new Exception('Failed unserialize data!');
        }

        return $dataArr;
    }


    public static function encodeUserToken()
    {
        $data = [
            'date' => date('Y-m-d'),
        ];

        return self::encodeToken($data);
    }

    public static function decodeUserToken($tokenBase64)
    {
        $dataArr = self::decodeToken($tokenBase64);

        if (empty($dataArr) || empty($dataArr['date'])) {
            throw new Exception('Failed unserialize data!');
        }

        if (strtotime($dataArr['date']) !== strtotime(date('Y-m-d'))) {
            throw new Exception('The access token has expired.');
        }

        return $dataArr;
    }
}
