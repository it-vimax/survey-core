<?php

namespace Survey\Core\Helper;

class RandomHelper
{
    public static function getUniqueRandomArrayItems(array $allItems, $quantity)
    {
        shuffle($allItems); // перемешиваем массив с элементами
        return array_slice($allItems, 0, $quantity); // получаем необходимое количество элементов
    }

    public static function generateFileName($fileName, $type, $id)
    {
        return $type . '_' . $id . '_' . time() . '_' . $fileName;
    }

    //TODO Убрать
    public static function getNowDateTime()
    {
        return date('Y-m-d H:i:s');
    }
}
