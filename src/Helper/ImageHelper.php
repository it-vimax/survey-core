<?php

namespace Survey\Core\Helper;

class ImageHelper
{
    const IMG_SAVE_PATH = './public/img/tests/';
    const IMG_GET_PATH = '/img/tests/';

    public static function deleteOldImg($fileName)
    {
        if (! empty($fileName) &&
            file_exists(self::IMG_SAVE_PATH . $fileName)
        ) {
            unlink(self::IMG_SAVE_PATH . $fileName);
        }
    }
}
