<?php

namespace Survey\Core\Helper;

class RbacRoles
{
    const ADMINISTRATOR = 'Administrator';

    const PERMISSION_MANAGER = 'Permission.manager';

    const ROLE_MANAGER = 'Role.manager';

    const USER_MANAGER = 'User.manager';

    const PROJECT_EASY_LEARN_MANAGER = 'Project.easy-learn.manage';

    const PROJECT_VOLIA_HOME_MANAGER = 'Project.volia-home.manage';

    const PROJECT_HR_MANAGER = 'Project.hr.manage';

    const PROJECT_MANAGER = 'Project.manage';

    const TEST_MANAGER = 'Test.manager';

    const TESTING_MANAGER = 'Testing.manager';

    const TESTING_TEST_MANAGER = 'Testing-test.manager';

    const TEST_PERSON = 'Test-person';
}
