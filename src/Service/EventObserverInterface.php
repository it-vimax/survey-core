<?php

namespace Survey\Core\Service;

interface EventObserverInterface
{
    public function handler($data);
}
