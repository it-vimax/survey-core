<?php

namespace Survey\Core\Service;

abstract class AbstractEventManager
{
    private $observers = [];

    public function __construct()
    {
        $this->observers['*'] = [];
    }

    private function initEventGroup($event = '*')
    {
        if (!isset($this->observers[$event])) {
            $this->observers[$event] = [];
        }
    }

    private function getEventObservers($event= '*')
    {
        $this->initEventGroup($event);
        $group = $this->observers[$event];
        $all = $this->observers['*'];

        return array_merge($group, $all);
    }

    public function attach(EventObserverInterface $observer, $event = '*')
    {
        $this->initEventGroup($event);

        $this->observers[$event][] = $observer;
    }

    public function detach(EventObserverInterface $observer, $event = '*')
    {
        foreach ($this->getEventObservers($event) as $key => $subscriber) {
            if ($subscriber === $observer) {
                unset($this->observers[$event][$key]);
            }
        }
    }
    
    public function notify($event = '*', $data = null)
    {
        /** @var EventObserverInterface $observer */
        foreach ($this->getEventObservers($event) as $observer) {
            $observer->handler($data);
        }
    }
}
