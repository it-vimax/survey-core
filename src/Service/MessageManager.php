<?php

namespace Survey\Core\Service;

use Doctrine\ORM\EntityManager;
use Survey\Core\Entity\MessageTemplate;
use Survey\Core\Entity\TestingTest;
use Survey\Core\Entity\User;
use Survey\Core\Entity\UserNotificationLog;
use Survey\Core\Helper\Log;
use Survey\Core\Utils\MessageShortCode;
use GuzzleHttp\Client;

class MessageManager
{
    private $entityManager;
    private $config;

    public function __construct(EntityManager $entityManager, $config)
    {
        $this->entityManager = $entityManager;
        $this->config = $config;
    }

    public function sendMessageToUser($userId, $templateId, $testingTestId, $employeeId = null)
    {
        $this->entityManager->getConnection()->connect();
        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $userId]);
        /** @var User $employee */
        $employee = null;
        if (!empty($employeeId)) {
            $employee = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $employeeId]);
        }
        /** @var MessageTemplate $template */
        $template = $this->entityManager->getRepository(MessageTemplate::class)->findOneBy(['id' => $templateId]);
        /** @var TestingTest $testingTest */
        $testingTest = $this->entityManager->getRepository(TestingTest::class)->findOneBy(['id' => $testingTestId]);
        $emailShortCode = new MessageShortCode(
            $this->entityManager,
            $user,
            $testingTest,
            getenv('FRONTEND_TEST_HR'),
            $employee
        );
        $template->initMessageShortCode($emailShortCode);

        $templateBody = "";
        if ($template->isFormatEmail()) {
            $templateBody .= "<!--TEMPLATE_HTML-->";
        }
        $templateBody .= $template->getBodyWithShortCode();
        $templateSubject = $template->getSubjectWithShortCode();

        $client = new Client();
        $result = $client->request(
            'POST',
            $this->config['message_service']['host'] . $this->config['message_service']['uri']['send_message'],
            [
                'headers' => [
                    "Content-Type" => "application/json",
                ],
                'auth' => [
                    $this->config['message_service']['username'],
                    $this->config['message_service']['password'],
                ],
                'json' => [
                    "messageType" => $template->getServiceManagerMessageType(),
                    "toAddress" => $user->getEmail(),
                    "message" => $templateBody,
                    "subject" => $templateSubject,
                    "fromAddress" => $this->config['project_email'],
                ],
            ]
        );
        
        if ($result->getStatusCode() !== 200) {
            throw new \Exception('Error send message to remote the message serivce. Body respose: ' . $result->getBody()->getContents());
        }

        // save log
        $userNotificationLog = new UserNotificationLog();
        $userNotificationLog->setUser($user);
        $userNotificationLog->setNotificationType($template->getNameFormat());
        $userNotificationLog->setSubject($templateSubject);
        $userNotificationLog->setMessage($templateBody);
        $userNotificationLog->setCreateAt();

        $user->addNotificationToLog($userNotificationLog);

        $this->entityManager->persist($userNotificationLog);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $this->entityManager->getConnection()->close();

        Log::sendMessageLog(
            $user->getEmail(),
            $template->getNameFormat(),
            $templateSubject,
            $templateBody
        );
    }
}
