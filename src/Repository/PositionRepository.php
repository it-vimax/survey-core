<?php

namespace Survey\Core\Repository;

use Doctrine\ORM\EntityRepository;
use Survey\Core\Entity\Department;
use Survey\Core\Entity\Position;
use Exception;

class PositionRepository extends EntityRepository
{
    public function findAll()
    {
        return $this->findBy([], ['name' => 'ASC']);
    }

    public function getById($id): Position
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('p')
            ->from(Position::class, 'p')
            ->where('p.id = :id')
            ->setParameter('id', $id);

        /** @var Position $position */
        $position = $queryBuilder->getQuery()->getOneOrNullResult();
        if (empty($position)) {
            throw new Exception('Position is not exist by id ' . $id);
        }

        return $position;
    }

    public function findAllByTestingTest($testingTestId)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('p')
            ->from(Position::class, 'p')
            ->leftJoin('p.users', 'u')
            ->leftJoin('u.testingTests', 'tt')
            ->where('tt.id = :tt_id')
            ->orderBy('p.name')
            ->setParameter('tt_id', $testingTestId);

        return $queryBuilder->getQuery()->getResult();
    }

    public function isNotExistInDepartment(Department $department)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();
        $expr = $entityManager->getExpressionBuilder();

        $query = $queryBuilder->select('p')
            ->from(Position::class, 'p')
            ->where($expr->notIn('p.id',
                $entityManager->createQueryBuilder()
                    ->select('p2.id')
                    ->from(Position::class, 'p2')
                    ->leftJoin('p2.departments', 'd')
                    ->where('d.id = :id')
                    ->getDQL()
                ))
            ->setParameter('id', $department->getId())
            ->orderBy('p.name', 'ASC')
            ->getQuery();

        return $query->getResult();
    }
}
