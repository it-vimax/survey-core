<?php

namespace Survey\Core\Repository;

use Survey\Core\Entity\Question;
use Survey\Core\Entity\Test;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Exception;

class QuestionRepository extends EntityRepository
{
    public function getQuestionById($questionId)
    {
        /** @var Question $question */
        $question = $this->find($questionId);
        if (empty($question)) {
            throw new Exception('The question ID ' . $questionId . ' is not exist!');
        }

        return $question;
    }

    public function getActiveQuestionById($questionId)
    {
        /** @var Question $question */
        $question = $this->findOneBy([
            'id' => $questionId,
            'state' => Question::STATE_ACTIVE,
        ]);
        if (empty($question)) {
            throw new Exception('The question ID ' . $questionId . ' is not active or exist!');
        }

        return $question;
    }
    
    public function findValueByTest(Test $test, $value)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('q')
            ->from(Question::class, 'q')
            ->join(Test::class, 't', Join::WITH, 'q.test = t.id')
            ->where('q.state <> ?1')
            ->andWhere('t.id = ?1')
            ->andWhere('q.value = ?2')
            ->orderBy('q.updateAt')
            ->setMaxResults(1)
            ->setParameter('1', $test->getId())
            ->setParameter('2', $value);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
    
    public function findAllDontRemoveByTest($testId)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('q')
            ->from(Question::class, 'q')
            ->join(Test::class, 't', Join::WITH, 'q.test = t.id')
            ->where('q.state <> ?1')
            ->andWhere('t.id = ?2')
            ->orderBy('q.updateAt')
            ->setParameter('1', Question::STATE_DELETE)
            ->setParameter('2', $testId);

        return $queryBuilder->getQuery()->getResult();
    }
    
    public function findAllActiveByTest($testId)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('q')
            ->from(Question::class, 'q')
            ->join(Test::class, 't', Join::WITH, 'q.test = t.id')
            ->where('q.state = ?1')
            ->andWhere('t.id = ?2')
            ->orderBy('q.updateAt')
            ->setParameter('1', Question::STATE_ACTIVE)
            ->setParameter('2', $testId);

        return $queryBuilder->getQuery()->getResult();
    }

    public function findOneNotRemoveQuestionByCodeInTest($code, Test $test)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('q')
            ->from(Question::class, 'q')
            ->join(Test::class, 't', Join::WITH, 'q.test = t.id')
            ->where('q.code = ?1')
            ->andWhere('t.id = ?2')
            ->andWhere('q.state <> ?3')
            ->setMaxResults(1)
            ->setParameter('1', $code)
            ->setParameter('2', $test->getId())
            ->setParameter('3', Question::STATE_DELETE);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
    
    public function findOtherActiveQuestionsByTest(Test $test, $oldQuestionList)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('q')
            ->from(Question::class, 'q')
            ->join(Test::class, 't', Join::WITH, 'q.test = t.id')
            ->where('q.state = ?1')
            ->andWhere('t.id = ?2')
            ->andWhere('q.id not in (:ids)')
            ->orderBy('q.updateAt')
            ->setParameter('1', Question::STATE_ACTIVE)
            ->setParameter('2', $test->getId())
            ->setParameter('ids', $oldQuestionList);

        return $queryBuilder->getQuery()->getResult();
    }
}
