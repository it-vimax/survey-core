<?php

namespace Survey\Core\Repository;

use Doctrine\ORM\EntityRepository;
use Survey\Core\Entity\UserFunction;

class UserFunctionRepository extends EntityRepository
{
    public function findAll()
    {
        return $this->findBy([], ['name' => 'ASC']);
    }

    public function findAllByTestingTest($testingTestId)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('uf')
            ->from(UserFunction::class, 'uf')
            ->leftJoin('uf.users', 'u')
            ->leftJoin('u.testingTests', 'tt')
            ->where('tt.id = :tt_id')
            ->orderBy('uf.name')
            ->setParameter('tt_id', $testingTestId);

        return $queryBuilder->getQuery()->getResult();
    }
}
