<?php

namespace Survey\Core\Repository;

use Doctrine\ORM\EntityRepository;
use Survey\Core\Entity\Department;
use Exception;

class DepartmentRepository extends EntityRepository
{
    public function getById($id): Department
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('d')
            ->from(Department::class, 'd')
            ->where('d.id = :id')
            ->setParameter('id', $id);

        /** @var Department $department */
        $department = $queryBuilder->getQuery()->getOneOrNullResult();
        if (empty($department)) {
            throw new Exception('Department by id: ' . $id . ' is not exist!');
        }
        
        return $department;
    }
    
    
    public function findAllWithOrganizations()
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('d, o')
            ->from(Department::class, 'd')
            ->leftJoin('d.organization', 'o')
            ->orderBy('d.name');

        return $queryBuilder->getQuery()->getResult();
    }

    public function findAllWithOrganizationsByTestingTest($testingTestId)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('d, o')
            ->from(Department::class, 'd')
            ->leftJoin('d.organization', 'o')
            ->leftJoin('d.users', 'u')
            ->leftJoin('u.testingTests', 'tt')
            ->where('tt.id = :tt_id')
            ->orderBy('d.name')
            ->setParameter('tt_id', $testingTestId);

        return $queryBuilder->getQuery()->getResult();
    }
}
