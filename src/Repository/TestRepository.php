<?php

namespace Survey\Core\Repository;

use Survey\Core\Entity\Test;
use Doctrine\ORM\EntityRepository;
use Exception;

class TestRepository extends EntityRepository
{
    public function getTestById($testId)
    {
        /** @var Test $test */
        $test = $this->find($testId);
        if (empty($test)) {
            throw new Exception('Test ID ' . $testId . ' is not exist!');
        }

        return $test;
    }

    public function findAllDontRemove()
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('t')
            ->from(Test::class, 't')
            ->where('t.state <> ?1')
            ->orderBy('t.updateAt')
            ->setParameter('1', Test::STATE_DELETE);

        return $queryBuilder->getQuery()->getResult();
    }
}
