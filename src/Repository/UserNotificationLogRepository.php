<?php

namespace Survey\Core\Repository;

use Doctrine\ORM\EntityRepository;
use Survey\Core\Entity\User;
use Survey\Core\Entity\UserNotificationLog;

class UserNotificationLogRepository extends EntityRepository
{
    public function findAllLogsByUser(User $user)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('unl')
            ->from(UserNotificationLog::class, 'unl')
            ->where('unl.user = :user')
            ->orderBy('unl.createAt', 'DESC')
            ->setParameter('user', $user);

        return $queryBuilder->getQuery();
    }
}
