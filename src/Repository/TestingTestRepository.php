<?php

namespace Survey\Core\Repository;

use Survey\Core\Entity\User;
use Survey\Core\Entity\Test;
use Survey\Core\Entity\Testing;
use Survey\Core\Entity\TestingTest;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Exception;

class TestingTestRepository extends EntityRepository
{
    public function getTestingTestById($testingTestId)
    {
        /** @var TestingTest $testingTest */
        $testingTest = $this->find($testingTestId);
        if (empty($testingTest)) {
            throw new Exception('The testing test ID ' . $testingTestId . ' is not exist!');
        }

        return $testingTest;
    }

    public function findOneActiveTestingTest()
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('tt')
            ->from(TestingTest::class, 'tt')
            ->where('tt.state = :stateActive')
            ->orWhere('tt.state = :stateDraft')
            ->setMaxResults(1)
            ->orderBy('tt.createAt', 'DESC')
            ->setParameter('stateActive', TestingTest::STATE_ACTIVE)
            ->setParameter('stateDraft', TestingTest::STATE_DRAFT);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findAllTodayStartedTests()
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('tt')
            ->from(TestingTest::class, 'tt')
            ->where('tt.state = :stateActive')
            ->orWhere('tt.state = :stateDraft')
            ->andWhere('tt.startTestAt = CURRENT_DATE()')
            ->orderBy('tt.createAt', 'DESC')
            ->setParameter('stateActive', TestingTest::STATE_ACTIVE)
            ->setParameter('stateDraft', TestingTest::STATE_DRAFT);

        return $queryBuilder->getQuery()->getResult();
    }
    
    public function findAllStartedTests()
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('tt')
            ->from(TestingTest::class, 'tt')
            ->where('tt.state = :stateActive')
            ->orWhere('tt.state = :stateDraft')
            ->andWhere('tt.startTestAt <= CURRENT_DATE()')
            ->andWhere('tt.finishTestAt > CURRENT_DATE()')
            ->orderBy('tt.createAt', 'DESC')
            ->setParameter('stateActive', TestingTest::STATE_ACTIVE)
            ->setParameter('stateDraft', TestingTest::STATE_DRAFT);

        return $queryBuilder->getQuery()->getResult();
    }

    public function getActiveTestingTestById($testingTestId)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('tt')
            ->from(TestingTest::class, 'tt')
            ->where('tt.state <> ?1')
            ->andWhere('tt.id = ?2')
            ->setParameter('1', TestingTest::STATE_DELETE)
            ->setParameter('2', $testingTestId);
        
        /** @var TestingTest $testingTest */
        $testingTest = $queryBuilder->getQuery()->getOneOrNullResult();

        if (empty($testingTest)) {
            throw new Exception('The testing test ID ' . $testingTestId . ' is not active!');
        }

        return $testingTest;
    }

    public function findAllDontRemoveByTesting($testingId)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('tt')
            ->from(TestingTest::class, 'tt')
            ->where('tt.state <> ?1')
            ->andWhere('tt.testing_id = ?2')
            ->orderBy('tt.updateAt')
            ->setParameter('1', TestingTest::STATE_DELETE)
            ->setParameter('2', $testingId);

        return $queryBuilder->getQuery()->getResult();
    }

    public function findLastTestByTesting($testId, $testingId)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('tt')
            ->from(TestingTest::class, 'tt')
            ->join(Test::class, 't', Join::WITH, 'tt.test = t.id')
            ->join(Testing::class, 'ts', Join::WITH, 'tt.testing = ts.id')
            ->where('tt.state <> ?1')
            ->andWhere('ts.id = ?2')
            ->andWhere('t.id = ?3')
            ->orderBy('tt.finishTestAt', 'DESC')
            ->setMaxResults(1)
            ->setParameter('1', TestingTest::STATE_DELETE)
            ->setParameter('2', $testingId)
            ->setParameter('3', $testId);

        return $queryBuilder->getQuery()->getResult();
    }

    public function filterTestingTestWithPaginationByUser(User $user, $data)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('tt')
            ->from(TestingTest::class, 'tt')
            ->leftJoin('tt.testing', 't')
            ->leftJoin('tt.users', 'u')
            ->where('u.id = :user_id')
            ->orderBy('tt.startTestAt', 'DESC')
            ->setParameter('user_id', $user->getId());

        if (isset($data['testing_id']) && !empty($data['testing_id'])) {
            $queryBuilder->andWhere('t.id = :testing_id');
            $queryBuilder->setParameter('testing_id', $data['testing_id']);
        }

        return $queryBuilder->getQuery();
    }
}
