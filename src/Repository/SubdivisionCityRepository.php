<?php

namespace Survey\Core\Repository;

use Doctrine\ORM\EntityRepository;
use Survey\Core\Entity\SubdivisionCity;

class SubdivisionCityRepository extends EntityRepository
{
    public function findAllWithCities()
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('s, c')
            ->from(SubdivisionCity::class, 's')
            ->leftJoin('s.city', 'c')
            ->orderBy('s.name');
        
        return $queryBuilder->getQuery()->getResult();
    }

    public function findAllWithCitiesByTestingTest($testingTestId)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('s, c')
            ->from(SubdivisionCity::class, 's')
            ->leftJoin('s.city', 'c')
            ->leftJoin('s.users', 'u')
            ->leftJoin('u.testingTests', 'tt')
            ->where('tt.id = :tt_id')
            ->orderBy('s.name')
            ->setParameter('tt_id', $testingTestId);

        return $queryBuilder->getQuery()->getResult();
    }
}
