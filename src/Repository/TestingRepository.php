<?php

namespace Survey\Core\Repository;

use Doctrine\ORM\EntityRepository;
use Exception;
use Doctrine\ORM\Query\Expr\Join;
use Survey\Core\Entity\Testing;
use Survey\Core\Entity\User;
use Survey\Core\Entity\TestingTest;

class TestingRepository extends EntityRepository
{
    public function getTestingById($testingId)
    {
        /** @var Testing $testing */
        $testing = $this->find($testingId);
        if (empty($testing)) {
            throw new Exception('The testing ID ' . $testingId . ' is not exist!');
        }

        return $testing;
    }
    
    public function findAllDontRemove($projectIds)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('s')
            ->from(Testing::class, 's')
            ->where('s.state <> ?1')
            ->andWhere('s.projectId IN (:ids)')
            ->orderBy('s.updateAt')
            ->setParameter('ids', $projectIds)
            ->setParameter('1', Testing::STATE_DELETE);

        return $queryBuilder->getQuery()->getResult();
    }

    public function findAllTestingByUser(User $user)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('t')
            ->from(Testing::class, 't')
            ->leftJoin('t.testingTests', 'tt')
            ->leftJoin('tt.users', 'u')
            ->andWhere('u.id = :user_id')
            ->orderBy('u.createAt', 'DESC')
            ->setParameter('user_id', $user->getId());

        return $queryBuilder->getQuery()->getResult();;
    }
}
