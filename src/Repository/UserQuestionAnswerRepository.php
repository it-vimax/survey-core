<?php

namespace Survey\Core\Repository;

use Survey\Core\Entity\TestingTest;
use Doctrine\ORM\EntityRepository;
use Survey\Core\Entity\UserQuestionAnswer;
use Survey\Core\Entity\User;

class UserQuestionAnswerRepository extends EntityRepository
{
    public function findCountUsersForTestingTest(TestingTest $testingTest)
    {
        /** @var EntityRepository $entityManager */
        $entityManager = $this->getEntityManager();
        $sql = "select
                  (select count(DISTINCT uqa.user_id)
                  from user_question_answers as uqa
                  where uqa.user_id in (select user_id from user_testing_test where testing_test_id = :ttId)
                  and uqa.testing_test_id = :ttId) as passed,
                       
                 (select count(DISTINCT user_id)
                    from user_testing_test as utt
                    left join users as u on u.id = utt.user_id
                    where testing_test_id = :ttId
                      and user_id not in (select DISTINCT uqa.user_id
                          from user_question_answers as uqa
                          where uqa.user_id in (select user_id from user_testing_test where testing_test_id = :ttId)
                            and uqa.testing_test_id = :ttId)
                      and u.status = :userStatus) as must_passed";

        $stmt = $entityManager->getConnection()->prepare($sql);

        $stmt->bindValue("ttId", $testingTest->getId());
        $stmt->bindValue("userStatus", User::STATUS_ACTIVE);
        $stmt->execute();
        $result = $stmt->fetch();

        return $result;
    }

    public function getUserIdForTestingTest(TestingTest $testingTest)
    {
        /** @var EntityRepository $entityManager */
        $entityManager = $this->getEntityManager();

        $sql = "select distinct user_id
          from user_question_answers as uqa
          where uqa.testing_test_id = :ttId";

        $stmt = $entityManager->getConnection()->prepare($sql);

        $stmt->bindValue("ttId", $testingTest->getId());
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function getUserQuestionAnswersByTestingTest($answerId, $testingTestId)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('uqa')
            ->from(UserQuestionAnswer::class, 'uqa')
            ->leftJoin('uqa.answer', 'a')
            ->leftJoin('a.question', 'q')
            ->leftJoin('q.test', 't')
            ->leftJoin('t.testingTests', 'tt')
            ->where('uqa.testingTest = ?1')
            ->andWhere('a.id = ?2')
            ->setParameter('1', $testingTestId)
            ->setParameter('2', $answerId);

        return $queryBuilder->getQuery()->getResult();
    }

    public function getLastAttemptUserAnswerItem(User $user, TestingTest $testingTest)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('uqa')
            ->from(UserQuestionAnswer::class, 'uqa')
            ->where('uqa.user = ?1')
            ->andWhere('uqa.testingTest = ?2')
            ->orderBy('uqa.attempt', 'DESC')
            ->setMaxResults(1)
            ->setParameter('1', $user->getId())
            ->setParameter('2', $testingTest->getId());

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function getAttemptUserAnswerItems(User $user, TestingTest $testingTest, $attempt)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('uqa')
            ->from(UserQuestionAnswer::class, 'uqa')
            ->where('uqa.user = ?1')
            ->andWhere('uqa.testingTest = ?2')
            ->andWhere('uqa.attempt = ?3')
            ->setParameter('1', $user->getId())
            ->setParameter('2', $testingTest->getId())
            ->setParameter('3', $attempt);

        return $queryBuilder->getQuery()->getResult();
    }

    public function findWithMaxAttempt(User $user, TestingTest $testingTest)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $uqaWithMaxAttempts = $entityManager->createQueryBuilder()
            ->select('uqa2')
            ->from(UserQuestionAnswer::class, 'uqa2')
            ->where('uqa2.user = ?1')
            ->andWhere('uqa2.testingTest = ?2')
            ->orderBy('uqa2.attempt', 'desc')
            ->setMaxResults(1)
            ->setParameter('1', $user->getId())
            ->setParameter('2', $testingTest->getId())
            ->getQuery()
            ->getOneOrNullResult();

        if (empty($uqaWithMaxAttempts))
            return false;

        $maxAttempt = $uqaWithMaxAttempts->getAttempt();

        $queryBuilder->select('uqa')
            ->from(UserQuestionAnswer::class, 'uqa')
            ->where('uqa.user = ?1')
            ->andWhere('uqa.attempt = ?3')
            ->andWhere('uqa.testingTest = ?2')
            ->setParameter('1', $user->getId())
            ->setParameter('2', $testingTest->getId())
            ->setParameter('3', $maxAttempt);

        return $queryBuilder->getQuery()->getResult();
    }
    
    public function findWithMaxCycle(TestingTest $testingTest, User $user)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();
        /** @var UserQuestionAnswer $uqaWithMaxCycles */
        $uqaWithMaxCycles = $entityManager->createQueryBuilder()
            ->select('uqa2')
            ->from(UserQuestionAnswer::class, 'uqa2')
            ->where('uqa2.user = ?1')
            ->andWhere('uqa2.testingTest = ?2')
            ->orderBy('uqa2.cycle', 'desc')
            ->setMaxResults(1)
            ->setParameter('1', $user->getId())
            ->setParameter('2', $testingTest->getId())
            ->getQuery()
            ->getOneOrNullResult();

        if (empty($uqaWithMaxCycles))
            return false;

        $maxCycle = $uqaWithMaxCycles->getCycle();

        $queryBuilder->select('uqa')
            ->from(UserQuestionAnswer::class, 'uqa')
            ->where('uqa.user = ?1')
            ->andWhere('uqa.cycle = ?3')
            ->andWhere('uqa.testingTest = ?2')
            ->setParameter('1', $user->getId())
            ->setParameter('2', $testingTest->getId())
            ->setParameter('3', $maxCycle);

        return $queryBuilder->getQuery()->getResult();
    }
}
