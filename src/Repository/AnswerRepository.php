<?php

namespace Survey\Core\Repository;

use Survey\Core\Entity\Answer;
use Survey\Core\Entity\Question;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Exception;

class AnswerRepository extends EntityRepository
{
    public function getAnswerById($answerId)
    {
        /** @var Answer $answer */
        $answer = $this->find($answerId);
        if (empty($answer)) {
            throw new Exception('Answer ID ' . $answerId . ' is not exist!');
        }

        return $answer;
    }
    
    public function findAllDontRemoveByQuestion(Question $question)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('a')
            ->from(Answer::class, 'a')
            ->where('a.question = ?1')
            ->andWhere('a.state <> ?2')
            ->orderBy('a.updateAt')
            ->setParameter('1', $question)
            ->setParameter('2', Answer::STATE_DELETE);

        return $queryBuilder->getQuery()->getResult();
    }
    
    public function findAllActiveTruthAnswerOfQuestion($questionId)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('a')
            ->from(Answer::class, 'a')
            ->join(Question::class, 'q', Join::WITH, 'a.question = q.id')
            ->where('q.id = ?1')
            ->andWhere('a.state = ?2')
            ->andWhere('a.point = ?3')
            ->orderBy('a.updateAt')
            ->setParameter('1', $questionId)
            ->setParameter('2', Answer::STATE_ACTIVE)
            ->setParameter('3', Answer::STATUS_TRUTH);

        return $queryBuilder->getQuery()->getResult();
    }
    
    public function findAllActiveLieAnswerOfQuestion($questionId)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('a')
            ->from(Answer::class, 'a')
            ->join(Question::class, 'q', Join::WITH, 'a.question = q.id')
            ->where('q.id = ?1')
            ->andWhere('a.state = ?2')
            ->andWhere('a.point = ?3')
            ->orderBy('a.updateAt')
            ->setParameter('1', $questionId)
            ->setParameter('2', Answer::STATE_ACTIVE)
            ->setParameter('3', Answer::STATUS_LIE);

        return $queryBuilder->getQuery()->getResult();
    }

    public function findByWeightInQuestion(Question $question, $weight)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('a')
            ->from(Answer::class, 'a')
            ->join(Question::class, 'q', Join::WITH, 'a.question = q.id')
            ->where('q.id = ?1')
            ->andWhere('a.weight = ?2')
            ->orderBy('a.updateAt')
            ->setMaxResults(1)
            ->setParameter('1', $question->getId())
            ->setParameter('2', $weight);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function removeAllByWeight(Question $question, $weight)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('a')
            ->from(Answer::class, 'a')
            ->where('a.question = ?1')
            ->andWhere('a.weight = ?2')
            ->setParameter('1', $question)
            ->setParameter('2', $weight);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
