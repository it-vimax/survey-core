<?php

namespace Survey\Core\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Exception;
use Survey\Core\Entity\TestingTest;
use Survey\Core\Entity\User;

/**
 * This is the custom repository class for User entity.
 */
class UserRepository extends EntityRepository
{
    /**
     * Retrieves all users in descending dateCreated order.
     * @return Query
     */
    public function findAllUsers()
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('u')
            ->from(User::class, 'u')
            ->orderBy('u.createAt', 'DESC');

        return $queryBuilder->getQuery()->getResult();
    }

    public function findOneActiveUser()
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('u')
            ->from(User::class, 'u')
            ->where('u.status = ?1')
            ->setMaxResults(1)
            ->orderBy('u.createAt', 'DESC')
            ->setParameter('1', User::STATUS_ACTIVE);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function findAllActiveUsers()
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('u')
            ->from(User::class, 'u')
            ->where('u.status = ?1')
            ->orderBy('u.createAt', 'DESC')
            ->setParameter('1', User::STATUS_ACTIVE);

        return $queryBuilder->getQuery()->getResult();
    }
    
    public function findActiveUsersByIds($ids)
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('u')
            ->from(User::class, 'u')
            ->where('u.status = :status')
            ->andWhere('u.id in (:ids)')
            ->orderBy('u.createAt', 'DESC')
            ->setParameter('status', User::STATUS_ACTIVE)
            ->setParameter('ids', $ids);

        return $queryBuilder->getQuery()->getResult();
    }

    public function findAllUsersByPaginations()
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('u, r')
            ->from(User::class, 'u')
            ->leftJoin('u.roles', 'r')
            ->orderBy('u.createAt', 'DESC');

        return $queryBuilder->getQuery();
    }

    public function getActiveUserByEmail($email)
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('u')
            ->from(User::class, 'u')
            ->where('u.email = ?1')
            ->andWhere('u.status = ?2')
            ->orderBy('u.createAt', 'DESC')
            ->setParameter('1', $email)
            ->setParameter('2', User::STATUS_ACTIVE);

        $result = $queryBuilder->getQuery()->getOneOrNullResult();
        if (empty($result)) {
            throw new Exception('User by email "' . $email . '" is not exist!');
        }

        return $result;
    }

    /**
     * @return User
     */
    public function getEverythingExcept(array $ids)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('u, r')
            ->from(User::class, 'u')
            ->leftJoin('u.roles', 'r')
            ->where('u.status = ?1')
            ->orderBy('u.createAt', 'DESC')
            ->setParameter('1', User::STATUS_ACTIVE);

        if (!empty($ids)) {
            $queryBuilder->andWhere('u.id NOT IN (?2)');
            $queryBuilder->setParameter('2', $ids);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function getUserById($userId)
    {
        /** @var User $user */
        $user = $this->find($userId);
        if (empty($user)) {
            throw new Exception('User by id ' . $userId . ' is not exist!');
        }

        return $user;
    }

    public function findTestingTestIdsByUser(User $user, $project, $status)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('u, t, tt')
            ->from(User::class, 'u')
            ->leftJoin('u.testingTests', 'tt')
            ->leftJoin('tt.testing', 't')
            ->where('u.status = ?1')
            ->andWhere('u.id = ?2')
            ->andWhere('t.projectId = ?3')
            ->andWhere('tt.state <> ?4')
            ->orderBy('u.createAt', 'DESC')
            ->setParameter('1', User::STATUS_ACTIVE)
            ->setParameter('2', $user->getId())
            ->setParameter('3', $project)
            ->setParameter('4', TestingTest::STATE_DELETE);

        switch ($status) {
            case User::TESTS_AWAITING:
                $queryBuilder->andWhere('tt.startTestAt > CURRENT_DATE()');
                break;

            case User::TESTS_COMPLETED:
                $queryBuilder->andWhere('tt.finishTestAt < CURRENT_DATE()');
                break;

            case User::TESTS_PASSES:
                $queryBuilder->andWhere('tt.startTestAt <= CURRENT_DATE()');
                $queryBuilder->andWhere('tt.finishTestAt >= CURRENT_DATE()');
                break;

            case User::TESTS_NOT_COMPLETED:
                $queryBuilder->andWhere('tt.finishTestAt >= CURRENT_DATE()');
                break;
        }

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function filterUsersByPaginations($filterData)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('u')
            ->from(User::class, 'u')
            ->where('u.status = ?1')
            ->orderBy('u.createAt', 'DESC')
            ->setParameter('1', User::STATUS_ACTIVE);

        if (isset($filterData['ids'])) {
            $queryBuilder->andWhere('u.id IN (:ids)');
            $queryBuilder->setParameter('ids', $filterData['ids']);
        }

        if (isset($filterData['user_name'])) {
            $queryBuilder->andWhere('u.fullName like :user_name');
            $queryBuilder->setParameter('user_name', '%' . $filterData['user_name'] . '%');
        }

        if (isset($filterData['user_email'])) {
            $queryBuilder->andWhere('u.email like :user_email');
            $queryBuilder->setParameter('user_email', '%' . $filterData['user_email'] . '%');
        }

        if (isset($filterData['subdivision_city_id'])) {
            $queryBuilder->andWhere('u.subdivisionCity = :subdivision_city_id');
            $queryBuilder->setParameter('subdivision_city_id', $filterData['subdivision_city_id']);
        }

        if (isset($filterData['department_id'])) {
            $queryBuilder->andWhere('u.department = :department_id');
            $queryBuilder->setParameter('department_id', $filterData['department_id']);
        }

        if (isset($filterData['position_id'])) {
            $queryBuilder->andWhere('u.position = :position_id');
            $queryBuilder->setParameter('position_id', $filterData['position_id']);
        }

        if (isset($filterData['user_function_id'])) {
            $queryBuilder->andWhere('u.userFunction = :user_function_id');
            $queryBuilder->setParameter('user_function_id', $filterData['user_function_id']);
        }

        return $queryBuilder->getQuery();
    }

    public function findAllUsersInTestingTestByPagination($testingTestId, $filterData = null)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('u, tt, r')
            ->from(User::class, 'u')
            ->leftJoin('u.testingTests', 'tt')
            ->leftJoin('u.roles', 'r')
            ->where('tt.id = :testing_test_id')
            ->andWhere('u.status = :status')
            ->orderBy('u.email', 'DESC')
            ->setParameter('testing_test_id', $testingTestId)
            ->setParameter('status', User::STATUS_ACTIVE)
            ->orderBy('u.email');

        if (isset($filterData['user_name'])) {
            $queryBuilder->andWhere('u.fullName like :user_name');
            $queryBuilder->setParameter('user_name', '%' . $filterData['user_name'] . '%');
        }

        if (isset($filterData['user_email'])) {
            $queryBuilder->andWhere('u.email like :user_email');
            $queryBuilder->setParameter('user_email', '%' . $filterData['user_email'] . '%');
        }

        if (isset($filterData['subdivision_city_id'])) {
            $queryBuilder->andWhere('u.subdivisionCity = :subdivision_city_id');
            $queryBuilder->setParameter('subdivision_city_id', $filterData['subdivision_city_id']);
        }

        if (isset($filterData['department_id'])) {
            $queryBuilder->andWhere('u.department = :department_id');
            $queryBuilder->setParameter('department_id', $filterData['department_id']);
        }

        if (isset($filterData['position_id'])) {
            $queryBuilder->andWhere('u.position = :position_id');
            $queryBuilder->setParameter('position_id', $filterData['position_id']);
        }

        if (isset($filterData['user_function_id'])) {
            $queryBuilder->andWhere('u.userFunction = :user_function_id');
            $queryBuilder->setParameter('user_function_id', $filterData['user_function_id']);
        }

        return $queryBuilder->getQuery();
    }
}
