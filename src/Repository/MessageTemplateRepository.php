<?php

namespace Survey\Core\Repository;

use Doctrine\ORM\EntityRepository;
use Exception;
use Survey\Core\Entity\MessageTemplate;

class MessageTemplateRepository extends EntityRepository
{
    public function getMessageTemplateById($messageTemplateId)
    {
        /** @var MessageTemplate $email */
        $email = $this->find($messageTemplateId);
        if (empty($email)) {
            throw new Exception('Message template ID ' . $messageTemplateId . ' is not exist!');
        }

        return $email;
    }
    
    public function getLastTemplateByType($type)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('mt')
            ->from(MessageTemplate::class, 'mt')
            ->where('mt.type = :type')
            ->orderBy('mt.updateAt')
            ->setMaxResults(1)
            ->setParameter('type', $type);

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    public function getTemplateByType($type)
    {
        $entityManager = $this->getEntityManager();
        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('mt')
            ->from(MessageTemplate::class, 'mt')
            ->where('mt.type = :type')
            ->orderBy('mt.updateAt')
            ->setParameter('type', $type);

        return $queryBuilder->getQuery()->getResult();
    }
}
